Sphinx Tryout!
==============
.. toctree::
   :maxdepth: 3
   :caption: How To Be A Better Developer

   reading/articles/threeDifferencesBetweenJuniorAndSeniorDeveloper

.. toctree::
   :maxdepth: 3
   :caption: Studies

   studies/pluralsight/design-patterns/structural/structuralPatterns.rst
   studies/matemaatika/matemaatiline_analüüs/loeng_1.rst
   studies/matemaatika/statistiline_analüüs/statistika.rst
   studies/matemaatika/lineaaralgebra/lineaaralgebra.rst

.. toctree::
   :maxdepth: 3
   :caption: TypeScript

   reading/typescript/fiveTipsForBetterTSCode

.. toctree::
   :maxdepth: 3
   :caption: JavaScript

.. toctree::
   :maxdepth: 3
   :caption: React

.. toctree::
   :maxdepth: 3
   :caption: Jest

.. toctree::
   :maxdepth: 3
   :caption: Varia

   /reading/varia/peerdep.rst

.. toctree::
   :maxdepth: 3
   :caption: Extras

   pages/licence
