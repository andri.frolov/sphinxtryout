==================================
Dependencies and Peer Dependencies
==================================
Mis on sõltuvus ja mis on peer dependency?

    Sõltuvused asuvad package.json-is nimekirjas dependencies.
    Peer dependencyd asuvad package.json-is nimekirjas peerDependencies.

**Sõltuvused**

    Kui lisad paketi sõltuvuste nimekirja, siis ütled:

    -   Minu kood vajab seda paketti töötamiseks.
    -   Kui seda paketti kaustas node_modules pole, siis lisa see automaatselt.
    -   Lisa ka paketid, mis on algselt lisatud paketi sõltuvuste nimekirjas. Neid pakette nimetatakse transitiivseteks sõltuvusteks.

**Peer dependency**

    Kui lisad paketi peer dependency nimekirja, siis ütled:

    -   Minu kood ühitub selle paketi selle versiooniga.
    -   Kui see pakett on kaustas node_modules juba olemas, siis ära tee midagi.
    -   Kui aga paketti seal pole või versioon on vale, siis ära lisa seda paketti, kuid hoiata kasutajat, et seda paketti ei leitud.

**Kuidas versioone kirjutada saab ja mida sümbolid tähendavad?**

    Variandid:

    ::

        "peerDependencies": {
            "mobx": "4.7.0",
            "mobx-react": "11.x.x",
            "react": ">= 16.14.0 < 17",
            "react-dom": "^16.0.3",
            "webpack": "~16.0.3"
        }

**Kuidas laheneb versioonikonflikt?**

    Reegel on järgmine:

    -   **npm lisab uue privaatse versiooni paketist, millega konflikt tekkis.**

    ::

        node_modules
        ├── lodash 4.17.11
        ├── todd-a 1.0.0
        ├── todd-b 1.0.0
        │   └── node_modules
        │       └── todd-child 2.0.0
        └── todd-child 1.0.0

**Miks on peer dependency hea?**

    Kui webpack su faile kokku pakib, siis on nende suurus väiksem.


Link: https://medium.com/angular-in-depth/npm-peer-dependencies-f843f3ac4e7f