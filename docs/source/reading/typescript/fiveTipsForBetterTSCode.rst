======================================================
Five Tips for Better TypeScript Code - Anthony Oleinik
======================================================
1. Null Coalescence ehk ?? fallback
-----------------------------------
Dokumentatsiooni definitsioon:

::

    The nullish coalescing operator is an alternative to ||
    which returns the right-side expression if the left-side
    is null or undefined.

**Miks see hea on?** Aitab vältida *null*-ist ja *undefined*-ist lähtuvate vigade viskamist.

?? üritab lahendada olukorda, kus potentsiaalse *null*-i või *undefined*-i tagastamine oleks maandatud mõne vaikeväärtuse või tegevusega.
Tavaolukorras võib tekkida mõne objekti väärtuse pärimisel viga:

::

    let t = myObj.property
    Cannot access property 'property' of undefined.

Küsimärk mõne objekti ja päringu vahel tähendab, et kui sellise päringu keti mõni lüli peaks olema tühi, siis TS peatab keti ja annab tagasi *undefined*.
Kett võib olla kuitahes pikk ja küsimärk võib olla iga objekti taga. See on nn *optional chaining*.
Allpool on *t* kas property väärtus või kui *myObj* on tühi, siis kett katkeb ja tagasi tuleb *undefined*.

::

    let t = myObj?.property
    t = property | undefined

?? annab meile võimaluse määrata vaikeväärtus undefinedi asemele.
Näiteks selles meetodis on võimatu anda kaasa määramata väärtust eeldusel, et ta võtab sisse stringi.

::

    sendFieldToServer(textField?.text ?? '')

TS-i dokumentatsiooni näide:

::

    let x = foo ?? bar();

Mis on võrdne sellega:

::

    let x = foo !== null && foo !== undefined ? foo : bar();

**NB!**
?? saab asendada || operaatorit vaikeväärtuse määramisel ja on parem, sest väldib || vahel ootamatut käitumist 0-i, NaN-i ja ""-ga, sest nad on *falsy* väärtused.

Näiteks selles koodis on bugi:

::

    function initializeAudio() {
      let volume = localStorage.volume || 0.5;
    }

Kui localStorage.volume on 0, siis paneb funktsioon ta 0,5-ks, mida me ei taha. ?? väldib seda.

Loe veel: https://devblogs.microsoft.com/typescript/announcing-typescript-3-7/

2. Kasuta tavalist eksporti default ekspordi asemel
---------------------------------------------------

::

    Halb: default export interface Dog { ... }
    Hea: export interface Dog { ... }


3. Use constrained string fields
--------------------------------

4. use Map<T>
--------------------------------

5. Figure out your eslint config / tsconfig
-------------------------------------------
