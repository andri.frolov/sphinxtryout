===================================================================
Three Differences Between a Junior & Senior Developer - Varun Joshi
===================================================================
Kõige tähtsamad on **oskused** ja **suhtumine**.

Joshi toob välja kolm soovitust, kus nooremarendaja võiks vanemarendajalt eeskuju võtta:

1. Ära torma koodima
--------------------
Nooremarendajad kipuvad ülesande kättesaamisel kohe koodima, vanemarendaja asub planeerima ning mõtleb lisaks happy flow'le välja ka kus võivad asjad nihu minna.
Ehk teisisõnu, nad mõtlevad lisaks happy flow'le ka nn edge-case'idele.

**Soovitused:**
    - Mõtle suurele pildile ja pane end kasutaja rolli.
    - Loo täpsem tegevusplaan ja mõtle, kus võib kasutaja raskustesse sattuda.
    - Võta aega ja tee korralikult, sest kiiruga tehtud kood on tõenäolisemalt vigane, mis tähendab rohkem tööd pärast töö esitamist.

2. Hoia kood sirgjooneline
--------------------------
Nooremarendajad kipuvad progema mulje avaldamiseks. Nad eelistavad keerulisi abstraktsioone ja üherealist koodi, mida on aga raske lugeda ja mõista.
Arvutit see ei sega, aga tiimikaaslasi küll. Kood peab olema inimesele loetav ja mõistetav ka nädalaid hiljem.

Vanemarendajad oskavad kirjutada koodi nii, et ka nooremarendaja saab sellest aru ning ta ei pea koodi talle selgitama. Kood selgitab end ise ja on lihtsasti loetav.

**Soovitused:**
    - Kirjuta koodi, mis on arusaadav ka rakendusega tutvuvale inimesele.
    - Keskendu koodi puhtusele, hoidu tarbetult mitmetasandilise koodi kirjutamisest.

3. Ole uuele avatud
-------------------
Tänapäeva IT on vaheldusrikas ning oodatakse, et arendaja töötab mitmes projektis mitme tehnoloogiaga.

Nooremarendajad kipuvad hoiduma uutele keeltele üleminekust, sest nad ei tea kuidas alustada ja kaua neil vahetuseks aega kulub.
Nad arvavad, et peavad kogu info omastama, enne kui julgevad midagi võtta. See on aga ajalõks.

Vanemarendaja võtab väljakutse vastu ning võtab asja äraõppimiseks aega.
Nad mõistavad, et kuigi keeled tulevad ja lähevad, siis progemise alus püsib suuresti sama.
See lubab neil vahetada keeli kiiremini kui nooremarendajad.

**Soovitused:**
    - Alusta algasjadest.
    - Tee mõned tutorialid ja õpi ülejäänu töö tegemise käigus.

*Link artiklile*: https://levelup.gitconnected.com/3-differences-between-a-junior-senior-developer-107179c6463c
