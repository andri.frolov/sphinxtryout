===================
Structural patterns
===================
Miks on mustrid tähtsad ja head?
--------------------------------
Annab arendajatele ühise keele probleemide lahendamiseks.
Probleemid arenduses korduvad ja mustrid lubavad neid lahendada efektiivselt.
Mõned mustrid sobivad mõnede probleemide lahendamiseks paremini kui teised. Igalühel neist on omad tugevused ja nõrkused.

Mustrite klasse on kolm:

    -   **Creational** ehk loov, mis keskendub objektide loomisele;
    -   **Structural** ehk strukturaalne, mis keskendub objektide kasutamise viisile.
    -   **Behavioral** ehk käitumuslik;


Creational
----------
Loovad mustrid on:

    -   ``Singleton``
    -   ``Builder``
    -   ``Prototype``
    -   ``Factory``
    -   ``AbstractFactory``

Singleton
~~~~~~~~~

Millal kasutada?
****************

-   Kui tahad tagada ühte instantsi kogu rakenduses.

Disain
******
Mis on ``Singleton``-i mõte?

    -   Lubab luua millestki vaid ühe eksemplari.
    -   Tagab kontrolli ressursi üle.
    -   Enamjaolt laaditakse ta laisalt.
    -   Näited: ``Runtime``, ``Logger``, ``Spring Beans``.

``Singleton`` vastutab iseenda loomise ja elutsükli haldamise eest ise.
Ta on loomult staatiline, kuid teda reeglina ei looda ``static`` klassina, sest ta peab olema thread-safe ja ``static`` seda meile ei taga.
``Singleton``-i sees on:

    -   ``private`` instants singletonist
    -   ``private`` konstruktor
    -   ``public`` meetod, mis instantsi välja kutsub.

UML
***

::

    -  singleton : Singleton
    ----------------------------
    -  Singleton()
    -  getInstance() : Singleton

Nii instants kui ka konstruktor on ``private``, sest tahame, et konstruktorit saab välja kutsuda ainult singleton ise.
Konstruktorisse parameetreid ei panda. Kui paned, siis on juba tegemist ``Factory`` mustriga ja rikud ``Singleton``-i reegleid.

Näide
*****
::

    Runtime singletonRuntime = Runtime.getruntime();
    singletonRuntime.gc();

    System.out.println(singletonRuntime);

    Runtime anotherInstance = Runtime.getruntime();

    System.out.println(anotherInstance);

    if(singletonRuntime == anotherInstance) {
        System.out.println("They are the same instance.");
    }

    // "They are the same instance."
    // Mäluaadress tuleb sama.

Harjutus
********
Põhi
____

::

    public class DbSingleton {
        private static DbSingleton instance = new DbSingleton();
        private DbSingleton() {}

        public static DbSingleton getInstance() {
          return instance;
        }
    }


-   Real kaks loome instantsi, mida meie rakendus saab kinni hoida.
-   Real kolm konstruktoriga haldame, et rakenduses on seda klassi instantsi vaid üks. Nii tagame, et keegi ei saa omapäi seda võtmesõnaga ``new`` uuesti luua.
-   Getteriga tagame viisi, kuidas singletoni saab välja kutsuda.

Lazy-loading
____________

Praegu on real 2 eagerly-loaded, sest loome uue instantsi hoolimata sellest, kas meil on seda vaja.

::

    public static DbSingleton getInstance() {
      if(instance == null) {
          instance = new DbSingleton();
      }
      return instance;
    }

``null``-checkiga pole singleton rakenduse tööleminemisel veel olemas, kuid sisu luuakse väljaküsimisel.

*Lazy-loading* aitab rakendusel kiiremini tööle minna, sest tal on vähem loomist.

Thread-safety
_____________
Kasutame võtmesõna ``volatile``, mis aitab tagada instantsi püsimise ``Singleton``-ina JVM-is. JVM ei taasloo teda.
See tagab, et keegi ei saaks kasutada meie koodi reflektiivselt.

::

    private DbSingleton() {
      if(instance != null) {
        throw new RuntimeException("Use getInstance() method to create.");
      }
    }

Lisame kaks asja: *double-checking lock mechanism* ja *synchronized check*.
Viimase puhul kasutan võtmesõna ``synchronize``.

NB! Sünkroniseeri instants, mitte meetod. Muidu saab efektiivsus suure hoobi.

::

    public static DbSingleton getInstance() {
      if(instance == null) {
          synchronized (DbSingleton.class) {
            if(instance == null) {
              instance = new DbSingleton();
            }
          }
      }

      return instance;
    }

Sügavaim ``null``-check käivitub vaid instantsi esmakordsel loomisel.

Lõpptulemus
___________

::

    public class DbSingleton {
        private static volatile DbSingleton instance = null;

        private DbSingleton() {
          if(instance != null) {
            throw new RuntimeException("Use getInstance() method to create.");
          }
        }

        public static DbSingleton getInstance() {
          if(instance == null) {
              synchronized (DbSingleton.class) {
                if(instance == null) {
                  instance = new DbSingleton();
                }
              }
          }

          return instance;
        }
    }

Puudujäägid
***********

-   Tihti kasutatakse liigselt, kiputakse tegema kõike ``Singleton``-iks, kuigi pole vaja.
-   Kui kõik on ``Singleton``-id, siis rakendus läheb aeglasemaks.
-   Neid on raske ``unit``-testida, sest nad ei anna välja ``interface``'i ning nende muutujad ja meetodid on ``private``.
-   Hooletul tegemisel pole nad *thread-safe*.
-   Vahel aetakse ``Factory`` mustriks, kui ``Singleton``-i meetoditele hakatakse kaasa andma parameetreid.
-   ``java.util.Calendar`` ei ole ``Singleton``, vaid pigem ``Prototype``, sest see annab igal väljakutsumisel uue instantsi.

Võrdlus ``Factory`` mustriga
****************************

+----------------------------------------------------------+----------------------------------------+
|                       Singleton                          |                Factory                 |
+==========================================================+========================================+
| Tagastab sama instantsi iga kord                         | Tagastab mitu erinevat tüüpi instantse |
+----------------------------------------------------------+----------------------------------------+
| Üks ``private`` konstruktor, mis ei võta sisendargumente | Mitu konstruktorit                     |
+----------------------------------------------------------+----------------------------------------+
| ``interface`` puudub                                     | ``interface``-id on olemas             |
+----------------------------------------------------------+----------------------------------------+
|                                                          | Kohandub keskkonnaga kergemini         |
+----------------------------------------------------------+----------------------------------------+

Kokkuvõte
*********

-   Tagab ühe instantsi.
-   Lihtne implementeerida.
-   Lahendab väga selgelt määratletud probleemi.
-   Ära liigkasuta. Kõik ei pea olema ``Singleton``.
-   Ära aja segi ``Factory`` mustriga.

Builder
~~~~~~~
Millal kasutada?
****************

-   Kui konstruktorites on palju parameetreid ja tahad teha objekti loomisel muutmatuks (``immutable``), s.t ilma setteriteta.

Disain
******

Mis on ``Builder``-i mõte?

-   Lahendab OOP-s sageli esineva probleemi: **millist konstruktorit kasutada**?

    -   Sageli ehitatakse mitu konstruktorit läbi parameetrite valiku, mida on hiljem keeruline hallata (*telescoping constructor*).

    -   ``Builder`` lahendab probleemi nii, et objekt luuakse konstruktori, mitte parameetritega.

-   Lubab hallata keerulisi konstruktoreid, kus on palju parameetreid.
-   Saad teha objekti muutmatuks loomise hetkel.
-   Saad vabaneda igale parameetrile setteri panemise vajadusest.
-   Saad kasutada *generics*-eid.
-   Lubab seada nn lepingu ehk mis on valiidne objekt, mis tal peab olema.
-   Näited: ``StringBuilder``, ``DocumentBuilder``, ``Locale.Builder``.

``Builder`` ehitatakse reeglina staatilise siseklassina (*static inner class*), sest ta tagastab instantsi objektist, mida ta ehitab.
Ta kutsub välja õige konstruktori oma ``state``-i pealt.

UML
***

.. image:: /images/Builder_UML.PNG
   :width: 120pt
   :align: center

Harjutus:
*********
Builder on omaenda konteiner kuniks me oma objekti loome.

1.  Lisame oma argumentideta konstruktori, sest me veel ei tea, mis me sinna sisse tahame.
    Konstruktor on hea, sest kui tahame panna kaasa nõutud parameetrid. Saame konstruktoriga vajaliku sisendi peale sundida (näites Builder()).

2.  Loome hulga meetodeid (nt public Builder bread(String bread)), mis näevad välja nagu konstruktorid, aga pole. **Siin on Builder mustri võti!**

    -   Saaksime kasutada ka ``enum``-eid.

    2.1 Tagastame ``this`` ehk selle ``Builder`` objekti, mida loome. Tagastab iseenda.

3. Ehitame välja ``Builder``-i enda. Selleks teeme ``public`` meetodi LunchOrder build(), mis tagastab uue tellimuse kasutades ``this`` konteksti.
See lubab meil kopeerida LunchOrder klassi meetodi LunchOrder(Builder builder) kaudu väärtused, mis me builderiga kaasa anname.

    -   See on hea, sest lubab meil jõustada muutujad ehk *builderi* ``interface``-i ilma setteriteta. Pane tähele, et LunchOrderis on vaid getterid. Ehk teisisõnu saame määrata väärtusi ilma setteriteta.
        Saame ``Bean``-i mudeli painduvuse koos konstruktorite lepingulise iseloomuga.

::

    public class LunchOrder {
        private final String bread;
        private final String condiments;
        private final String dressing;
        private final String meat;

        private LunchOrder(Builder builder) {
            this.bread = builder.bread;
            this.condiments = builder.condiments;
            this.dressing = builder.dressing;
            this.meat = builder.meat;
        }

        public String getBread() {
            return bread;
        };

        public String getCondiments() {
            return condiments;
        };
        public String getDressing() {
            return dressing;
        };
        public String getMeat() {
            return meat;
        };
    }

::

    public static class Builder {
        private String bread;
        private String condiments;
        private String dressing;
        private String meat;

        public Builder() {}

        public LunchOrder build() {
            return new LunchOrder(this);
        };

        public Builder bread(String bread) {
            this.bread = bread;
            return this;
        }

        public Builder condiments(String condiments) {
            this.condiments = condiments;
            return this;
        }

        public Builder dressing(String dressing) {
            this.dressing = dressing;
            return this;
        }

        public Builder meat(String meat) {
            this.meat = meat;
            return this;
        }
    }

::

    public class BuilderEverydayDemo {
        public static void main(String[] args) {

            LunchOrder.Builder builder = new LunchOrder.Builder();
            builder.bread("Wheat").condiments("Lettuce").dressing("Mayo").meat("Turkey");
            LunchOrder lunchOrder = builder.build();

            System.out.println(lunchOrder.getBread());
            System.out.println(lunchOrder.getCondiments());
            System.out.println(lunchOrder.getDressing());
            System.out.println(lunchOrder.getMeat());
        }
    }

    >>> Wheat
    >>> Lettuce
    >>> Mayo
    >>> Turkey

Saab teha ka nii, et üks variant jääb välja. Hea, sest me ei pea tegema *edge-case* juhtumeid.

::

    public class BuilderEverydayDemo {
        public static void main(String[] args) {

            LunchOrder.Builder builder = new LunchOrder.Builder();
            builder.bread("Wheat").dressing("Mayo");
            LunchOrder lunchOrder = builder.build();

            System.out.println(lunchOrder.getBread());
            System.out.println(lunchOrder.getCondiments());
            System.out.println(lunchOrder.getDressing());
            System.out.println(lunchOrder.getMeat());
        }
    }

    >>> Wheat
    >>> null
    >>> Mayo
    >>> null

``null``-ide vältimiseks saab panna ka vaikeväärtusi.

Puudujäägid
***********

-   Loodud objektid on ``immutable``.
-   Mustrit rakendatakse staatilise siseklassiga (*static inner class*). Sellest saab mööda.
-   Erinevalt ``Prototype`` mustrist ``Builder`` mustrit reeglina sisse ei refaktota. Tema ümber disainitakse.
-   ``Builder`` on pisut keerulisem kui lihtsalt konstruktori kaudu tegemine.
-   Keerukust suurendab ka iseenda tagastamine igal väljakutsel.

    -   "People are not used to an object returning itself for each subsequent call."

Võrdlus ``Prototype`` mustriga
******************************

+-----------------------------------------------------------+
| Builder                                                   |
+===========================================================+
| Tegeleb keeruliste konstruktoritega                       |
|                                                           |
+-----------------------------------------------------------+
| Võib olla eraldi klass,                                   |
| kuid reeglina asub ta klassis, mida ta ehitab.            |
|                                                           |
| Kui ta panna eraldi klassi,                               |
| siis ühildub hästi *legacy* koodiga.                      |
+-----------------------------------------------------------+
| ``interface``-i pole vaja, kuid seda saab implementeerida.|
+-----------------------------------------------------------+

+---------------------------------------------+
| Prototype                                   |
+=============================================+
| Implementeeritakse kloonmeetodi ümber,      |
| millega välditakse keerulisi konstruktoreid.|
+---------------------------------------------+
| Raske implementeerida *legacy* koodis,      |
| kuna kloonmeetod keskendub muutujatele.     |
|                                             |
| ja konstruktoritele ning seetõttu asub      |
| klassis, mida ta kloonib.                   |
+---------------------------------------------+

Mõlemad keskenduvad keerulistele konstruktoritele ühe klassi sees, kuid lahendavad probleemi erinevalt.
``Builder`` üritab töötada koos keeruliste konstruktoritega, ``Prototype`` üritab vältida nende uuestikutsumist.

Kokkuvõte
*********

-   Loov viis kuidas saada hallata konstruktorite ja objektide loomise keerukust.
-   Üsna kerge implementeerida.
-   Miinuseid on vähe.
-   Saab refaktoda eraldi klassi. Tavaliselt on aga pandud staatilise siseklassina sellesse klassi, mida ta ehitab.

Prototype
~~~~~~~~~

Millal kasutada?
****************

-   Kui tahad vältida kulukat objekti loomist ja saad vajaliku liikmesmuutujate kopeerimisega.

Disain
******
Mis on ``Prototype``-i mõte?

-   Saada unikaalne instants samast objektist, tavaliselt kloonides.
-   Väldib alamklasside loomist.
-   Ei kasuta võtmesõna ``new``.
-   Tihti kasutavad ``Interface``'i.
-   Tavaliselt implementeeritakse registriga.

    -   Algne loodud objekt pannakse registrisse hoiule ja kloonitakse sealt.

-   Näited: java.lang.Object#clone().

``Prototype`` reeglina kasutab endas meetodit ja ``interface``'i Clone/Cloneable.
See lubab meil vältida võtmesõna ``new``. Tavalisel muutub loomine kulukaks siis, kui kutsume välja ``new``.

Kuigi ``Prototype`` kloonib objekti, siis **iga instants on ainulaadne**.

Erinevused ``Builder`` ja ``Singleton`` mustritest.

-   Erinevalt ``Builder``-ist ei halda kulukat loomist klient. Sesmõttes on ``Builder`` ``Prototype`` mustri vastand.
-   Erinevalt ``Singleton``-ist saab kasutada objekti konstrueerimisel parameetreid, kuid tavaliselt seda ei tehta.

Shallow vs Deep Copy

-   Shallow Copy kopeerib kõik *immediate property*-d.
-   Deep Copy kopeerib ka kõik objekti referentsid.

UML
***

.. image:: /images/Prototype_UML.PNG
   :width: 120pt
   :align: center

Harjutus
********

-   Loome ``Protoype`` mustri.
-   Näitame SC versiooni.
-   Loome objekti registri abiga.

``Prototype`` mustri süda on **register**, täpsemalt meetod **createItem**.
Peame *castima* ``Item``-iks, sest items.get(type).clone() tagastab objekti tüüpi ``Object`` (vt klassi ``Item`` meetod ``clone()``.
Probleem selles, et ``Cloneable`` tehti Java 1.0-is, mis ei tea mis on ``Generics``.

::

    import java.util.HashMap;
    import java.util.Map;

    public class Registry {
        private Map<String, Item> items = new HashMap<String, Item>();

        public Registry() {
            loadItems();
        }

        public Item createItem(String type) {
            Item item = null;

            try {
                item = (Item) (items.get(type)).clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }

            return item;
        }

        private void loadItems() {
            Movie movie = new Movie();
            movie.setTitle("Basic Movie");
            movie.setPrice(24.99);
            movie.setRuntime("2 hours");
            items.put("Movie", movie);

            Book book = new Book();
            book.setNumberOfPages(335);
            book.setPrice(19.99);
            book.setTitle("Basic Book");
            items.put("Book", book);
        }
    }

Item klass. See on ``abstract``, sest tahame, et ``Book`` ja ``Movie`` objektid rakendaksid Itemi lõplikku funktsionaalsust.

Peame panema klassi implementeerima ``interface``'i ``Cloneable``
See tähendab, et peame implementeerima ka tema meetodi ``clone()``. See tagastab objekti tüüpi ``Object``.
Me ei pea tegema midagi lisaks klassis ``Movie`` ega ``Book``.

::

    public abstract class Item implements Cloneable {
        private String title;
        private double price;
        private String url;

        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

Klass ``Movie``:

::

    public class Movie extends Item {
        private String runtime;

        public String getRuntime() {
            return runtime;
        }

        public void setRuntime(String runtime) {
            this.runtime = runtime;
        }
    }

Klass ``Book``;

::

    public class Book extends Item {
        private int numberOfPages;

        public int getNumberOfPages() {
            return numberOfPages;
        }

        public void setNumberOfPages(int numberOfPages) {
            this.numberOfPages = numberOfPages;
        }
    }

Demoklass:

Jooksutades laeme esmalt sisse registri ja sealt loadItems(), mis loob prototüübid ja laeb need registrisse.
Registris palume luua objekti "Movie", mis on unikaalne, ning siis seame selle väljad.
Pane tähele, et registris oleval prototüübil on vaikeväärtused (näiteks runtime väli, mida me ei seadista ja mis püsib kahel tunnil).
Registrist küsime soovitud objekti tüüpi välja võtmega. Lektor soovitab siin kasutada ``enum`` väärtusi.

::

    public class PrototypeDemo {
        public static void main(String[] args) {
            Registry registry = new Registry();
            Movie movie = (Movie) registry.createItem("Movie");
            movie.setTitle("Creational Patterns in Java");

            System.out.println(movie);
            System.out.println(movie.getRuntime());
            System.out.println(movie.getTitle());
            System.out.println(movie.getUrl());

            Movie anotherMovie = (Movie) registry.createItem("Movie");
            anotherMovie.setTitle("Gang of Four");

            System.out.println(anotherMovie);
            System.out.println(anotherMovie.getRuntime());
            System.out.println(anotherMovie.getTitle());
            System.out.println(anotherMovie.getUrl());
        }
    }

    >>> Prototype.Movie@6e8cf4c6
    >>> 2 hours
    >>> Creational Patterns in Java
    >>> null
    >>> Prototype.Movie@12edcd21
    >>> 2 hours
    >>> Gang of Four
    >>> null

Puudujäägid
***********

-   Sageli ei kasutada, sest pole selge millal kasutada.
-   Tihti tuleb kasutada koos muu mustriga ja alati registriga.
-   Sageli tahad DC-d, kuid ``Cloneable`` teeb vaid SC. DC tuleb ise implementeerida.

Võrdlus ``Factory`` mustriga
****************************

-   ``Prototype`` keskendub objekti kergekaalulisele loomisele, kas konstruktori koopia või kloonimisega.
-   ``Prototype``-iga soovid teha koopiat iseendast.

-   ``Factory`` seevastu keskendub objektide paindlikule loomisele vastavalt soovile.
-   ``Factory`` saab kasutada mitut konstruktorit
-   ``Factory`` loob konkreetse ja värske objekti, sest kasutame võtmesõna ``new``. Seega programmaatilisi vaikeväärtusi pole.

Kokkuvõte
*********

-   Tagame unikaalse instantsi iga kord kui küsime seda.
-   Sageli refaktoreeritakse sisse, et aidata rakenduse ``performance`` meetrikaga. Et saaksime objektid kiiremini.
-   Enne ``Factory`` mustrile minemist kaalu enne ``Prototype``-i. On olukordi, kus ``Prototype`` sobib paremini.

Factory
~~~~~~~

Millal kasutada?
****************

-   Kui tahad, et objekti loomise loogika ei paistaks kliendile välja. Selleks lükatakse loomine alamklassidele. Klient näeb vaid ühist ``interface``-i.
-   Kui tahad lepingut, mis täpsustab kuidas objektid raamistikus implementeeritakse, kuid annab rakendamise viisi voli lõppkasutajale.
-   Näited: Calendar, ResourceBundle, NumberFormat.

Disain
******

-   Factory vastutab instantside loomise ja on nende elutsükli loomise osa.
-   Objekte luuakse läbi ühise ``interface``-i.
-   Factory viitab ka konkreetsetele klassidele, mida klient ei näe, sest nendele viidatakse läbi ühise ``interface``-i.
-   Objekti küsimise, loomise meetod on sageli parameetritega. Objekt luuakse konkreetses klassis.

UML
***

Factory on abstraktne klass staatilise *factory* meetodiga, mis kutsub välja objekti loova konkreetse klassi *factory* meetodi.

.. image:: /images/Factory_UML.PNG
   :width: 120pt
   :align: center

Harjutus
********
