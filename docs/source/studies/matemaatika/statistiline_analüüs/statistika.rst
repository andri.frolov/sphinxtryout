Statistiline analüüs
#####################
Sissejuhatus
============
**Tõenäosusteooria** on teadus seaduspäradest juhuslike sündmuste ja juhuslike protsesside maailmas.
Ehk tõenäosusteooria uurib massiliselt toimuvates juhuslikes sündmustes esinevaid seaduspärasusi.
Ta on mitme teadusharu alus, meie kursusel – matemaatilisele statistikale.

TT uurib sündmusi, mille esinemine ühel katsel on ettearvamatu, kuid mille esinemissagedus pikas katseseerias on ennustatav.
Üksiksündmusega TT meid ei aita, vaid tegeleb massiliste sündmustega.

**Statistika** on teadus andmete kogumisest, töötlemisest ja statistiliselt korrektsete
järelduste tegemisest.

Statistika on laialivalguv termin. Matemaatilises statistika hõlmab statistikast vaid ühte väiksemat osa - kuidas valimi kohta järeldusi teha.
Põhineb täiesti tõenäosusteoorial ja on tõenäosusteooria rakendusvaldkond.

Tõenäosusteooria põhimõisted
============================
Sündmus
-------
Sündmus on algmõiste ehk teda ei saa defineerida lihtsamate mõistete abil, vaid saame ainult kirjeldada.
Tema puhul saab tõdeda, kas ta toimub või mitte. Kui tekib kahtlus, kas see on TT mõistes sündmus, siis mõtle,
kas suudad ära tabada sündmus. Sündmus on kirjeldatud, ta pole kunagi ühesõnaline. Kontekst tuleb juurde kirjutada.

Sündmused on näiteks:

    -   vesi hakkab keema normaalse õhurõhu juures temperatuuril 100C
    -   täringu veeretamisel tuleb kuus silma
    -   mündi viskamisel saame samaaegselt kulli ja kirja

Sündmus saab toimuda teatud tingimustes: sündmuse toimumiseks peab olema täidetud teatud tingimuste kompleks.
Tingimuste kompleksi täitmist nimetatakse **katseks**.

Sündmuste liigitamine
---------------------
Võimalikkuse määra järgi
^^^^^^^^^^^^^^^^^^^^^^^^
Üks võimalus sündmusi liigitada on võimalikkuse määra järgi. Siis on sündmusi kolm: kindel, võimatu ja juhuslik sündmus.

    -   Sündmus, mis antud katsel alati toimub, on **kindel sündmus**. Tähis :math:`\Omega`
    -   Sündmus, mis antud katsel ei saa kunagi toimuda, on **võimatu sündmus**. Tähis :math:`\emptyset`
    -   Sündmus, mis antud tingimustes võib toimuda või ka mitte, on **juhuslik sündmus**.

Sündmusi tähistatakse ladina tähestiku algusosa suurtähtedega: :math:`A, B, C \text{, või } A_1, A_2, ...`.

Võrdvõimalikkuse ja teineteise välistamise järgi
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Juhuslikke sündmusi nimetatakse **võrdvõimalikeks**, kui ühel neist ei ole rohkem võimalusi esiletulekuks kui teisel.

Näiteks: A - mündi viskamisel saame kulli, B - mündi viskamisel saame kirja.

Juhuslikud sündmused on **teineteist välistavad**, kui nad ei saa korraga toimuda.

Näiteks: A - täringu veeretamisel tuleb viis silma, B - täringu veeretamisel tuleb kaks silma.

Kui ühe sündmuse toimumisega saab kaasneda teise toimumine, siis nad on **teineteist mittevälistavad**.

Sündmuste täielik süsteem
-------------------------
Olgu ühel katsel n mõeldavat erinevat katsetulemust :math:`A_1 ... A_n`. Kusjuures üks nendest toimub kindlasti.
Juhul kui neist ühe toimumine välistab teiste samaaegse toimumise, räägitakse **täielikust sündmuste süsteemist**.

Täidetud peab olema kolm tingimust:

    1.  Katsel on n mõeldavat tulemust. Näiteks täringu veeretamisel on 6 mõeldavat katsetulemust.
    2.  Üks tulemustest toimub kindlasti
    3.  Ühe toimumine välistab teise samaaegse toimumise.

Täielikus süsteemis on teada kõik väärtused.

Sündmuste elementaarsündmuste süsteem
-------------------------------------
Kui täielikku sündmuste süsteemi kuuluvad üksiksündmused on võrdvõimalikud, siis nimetatakse neid **elementaarsündmusteks**.
Võrdvõimalike sündmuste täielikku süsteemi nimetatakse **elementaarsündmuste süsteemiks**.

Vastandsündmused
----------------
Sündmuse A vastandsündmus :math:`\invlogit{A}` on sündmus, mille toimumine seisneb sündmuse A mittetoimumises.
Sama mis inversioon diskreetses matemaatikas.

Sündmuse summa ja korrutis
--------------------------
Sama mis diskmatis loogiline liitmine (disjunktsioon) ja loogiline korrutamine (konjunktsioon) või hulgateoorias ühend ja ühisosa.
Kahe sündmuse A ja B summaks (ka ühendiks) ehk A+B nimetatakse sündmust, mille toimumine seisneb kas A või B või mõlema toimumises.

Kahe sündmuse A ja B korrutiseks (ka ühisosaks) ehk AB nimetatakse sündmust, mille toimumine seisneb mõlema sündmuse A ja B toimumises.

Analoogselt hulkadega kujutatakse sündmusi ja tehteid nendega Venni diagrammide abil.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/venn_summajakorrutis.PNG
   :width: 300pt
   :align: center

Summat või korrutist moodustav sündmus on **osasündmus**.

Sündmuste summa ja korrutise mõiste laiendatakse ka enama kui kahe sündmuse juhule.

Sündmuste :math:`A_1, A_2, ..., A_k \text{ summa } A_1 U A_2 U ... U A_k` toimub siis,
kui toimub ükskõik milline osasündmustest :math:`A_i (i = 1, 2, ..., k)`.

Sündmuste :math:`A_1, A_2, ..., A_k \text{ summa } A_1 \And A_2 \And ...\And A_k` toimub siis,
kui toimuvad kõik osasündmused :math:`A_i (i = 1, 2, ..., k)`.

Sündmuste tõenäosus (klassikaline)
----------------------------------
Klassikaline seepärast, et ta kauges ajaloos oli algselt selline. Tõenäosuse tähis on :math:`P` ehk Probability.

Sündmuse A tõenäosuseks :math:`P(A)` nimetatakse sündmuse toimumiseks soodsate juhuste arvu m suhet kõigi võimalike
juhuste arvusse n, **kus juhused moodustavad elementaarsündmuste süsteemi** ehk süsteem on täielik ja on täidetud
kõik kolm vajalikku tingimust.

Valem:

    :math:`P(A) = \frac{m}{n}`

Tõenäosuse klassikalisest definitsioonist järeldub, et:

    1. kindla sündmuse tõenäosus :math:`P(\Omega) = 1`
    2. võimatu sündmuse tõenäosus on :math:`P(\emptyset) = 1`
    3. juhusliku sündmuse tõenäosus on :math:`10 < P (A) < 1`
    4. sündmuse tõenäosus on alati rajades :math:`0 \leq P(A) \leq 1`

Tihti esitatakse tõenäosuse väärtus protsentides. Kindlalt toimuva sündmuse tõenäosus on 1, ehk see toimub 100%-lise
tõenäosusega.

Statistiline tõenäosus
----------------------
Statistiline tõenäosus pole midagi muud kui suhteline sagedus.

Läheb vaja siis, kui ei saa elementaarsüsteemi määrata. Nt kui pool täringut on puust ja pool tinast.

Alati ei ole võimalik elementaarsündmuste süsteemi määrata. Sündmuse tõenäosus leitakse sel korral katseliselt.
Kordugu sündmus :math:`A_n` katsest koosnevas seerias m korda. Sündmuse sageduse m ja katsete üldarvu n jagatist nimetatakse
**sündmuse suhteliseks (relatiivseks) sageduseks**:

Valem:

    :math:`w = \frac{m}{n}`

Suhteline sagedus kipub stabiliseeruma.

Suhteline sagedus w sõltub katseseeria pikkusest n. Katsete arvu kasvades on suhtelisel sagedusel tendents stabiliseeruda,
s.t. küllalt pikkades katseseeriates kõigub w teatud kindla arvu läheduses, mis võetaksegi **statistiliseks tõenäosuseks**:

Valem:

    :math:`P(A) \approx w = \frac{m}{n}`

Järeldusi statistilisest tõenäosusest
-------------------------------------
    1.  Kui statistiline tõenäosus võrdub ühega, siis ei tarvitse sündmus veel olla kindel.
    2.  Kui aga sündmus on kindel, siis see toimub igal üksikkatsel ja kindlasti statistiline tõenäosus tuleb 1.
    3.  Kui statistiline tõenäosus võrdub nulliga, siis ei tarvitse sündmus veel olla võimatu. Seeria pikendamisel võib sündmus toimuda.
    4.  Kui aga sündmus on võimatu, siis see ei toimu ühelgi üksikkatsel ja statistiline tõenäosus tuleb 0.

Geomeetriline tõenäosus
-----------------------
Näiteks on noolemäng, mille märklaud on pindalaga S ja milles on osapiirkond pindalaga s.
Vaatleme punkti "juhuslikku viskamist" piirkonda S.

A - Tabame märklaua osapiirkonda. Tõenäosus, et juhuslik punkt satub ühtlasi piirkonda s, avaldub piirkondade pindalade suhtena:

    :math:`P(A) = \frac{s}{S}`

Tõenäosuse arvutamise põhivõtted
================================
Sündmuste summa tõenäosus
-------------------------
Kahe **teineteist välistava** sündmuse summa tõenäosus on võrdne
osasündmuste tõenäosuste summaga: :math:`P(A U B) = P(A) + P(B)`

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/sündmuste_summa.PNG
   :width: 300pt
   :align: center

Kõigi võimaluste arv olgu *n*.
*A* jaoks soodsate juhuste arv *k*.
*B* jaoks soodsate juhuste arv *m*.

    :math:`P(A U B) = \frac{k + m}{n} = \frac{k}{n} + \frac{m}{n} = P(A) + P(B)`

Kui üksteist välistavaid sündmusi on palju, siis:

    :math:`P(A_1 U A_2 U ... U A_k ) = \sum_{i = 1}^k P(A_i)`

Ehk summa tõenäosus on võrdne tõenäosuste summaga.

Kahe teineteist **mittevälistava sündmuse** summa tõenäosus on võrdne osasündmuste tõenäosuste summaga, millest on lahutatud
osasündmuste koosesinemise ehk korrutise tõenäosus:

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/mv_sündmuste_summa.PNG
   :width: 300pt
   :align: center

Kuna sündmused on mittevälistavad, siis nad võivad toimuda korraga. Mahalahutamine toimub topeltlugemise välistamiseks.
Kui unustad maha lahutada, siis tuleb tõenäosus üle ühe.

**Kolme** üksteist mittevälistava sündmuse korral:

    :math:`P(A U B U C) = P(A) + P(B) + P(C) - P(AB) - P(AC) - P(BC) + P(ABC)`

Juurdeliitmine tuleb põhjusel, et lahutamiste tulemusena jääb ala P(ABC) arvestamata, mistõttu peame ta lisama.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/mv_kolm_sündmust.PNG
   :width: 300pt
   :align: center

Seda valemit väga ei kasutata. Selle asemel minnakse üle vastandsündmusele.

Järeldused
^^^^^^^^^^
    1.  Täieliku sündmuste süsteemi moodustavate sündmuste tõenäosuste summa on üks.
    2.  Sündmuse ja selle vastandsündmuse tõenäosuste summa on 1,
        sest sündmus ja tema vastandsündmus moodustavad ka täieliku süsteemi.

    .. image:: /studies/matemaatika/statistiline_analüüs/pildid/sündmuse_ja_vastandsündmuse_summa.PNG
       :width: 200pt
       :align: center

    3.  Ülesannetes otsitakse sageli sündmuse tõenäosust vastandsündmuse kaudu.

    .. image:: /studies/matemaatika/statistiline_analüüs/pildid/ülesannetes_sageli.PNG
       :width: 200pt
       :align: center

Abimõiste: Sõltuvad sündmused
-----------------------------
Sündmust B nim. sõltuvaks sündmusest A, kui sündmuse B toimumise tõenäosus sõltub sellest, kas sündmus A toimus või ei
toimunud.

Näide: “Blackjack’i” mängides tõmmatakse kaardipakist järjest kaarte neid tagasi panemata. Iga järgmise kaardi tõmbamisel
sõltub tulemus sellest, millised kaardid on juba välja tõmmatud.

Abimõiste: Tinglikud sündmused
------------------------------
Sündmuse B toimumise tõenäosust tingimusel, et toimus sündmus A, tähistatakse :math:`P (B | A)` ja nimetatakse sündmuse B
tinglikuks tõenäosuseks tingimusel A. **NB!** Eeldustingimus käib kriipsu taha!

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/tinglikud_sündmused_näide.PNG
   :width: 300pt
   :align: center

Toimumise puhul võeti punane kuul ning kuule on järgi 4. Kollaseid on järgi 2 ja punaseid ka 2.
Mittetoimumise puhul võeti ära kollane kuul, punaseid kuule on jätkuvalt 3 ning kuule järgi 4.

Järeldus
^^^^^^^^
Sündmus B on sõltumatu sündmusest A, kui :math:`P (B | A) = P (B)` ning sündmus B sõltub sündmusest A kui
:math:`P (B | A) \neq P (B)`.

Tõenäosuste korrutamine
-----------------------
Kahe mistahes sündmuse A ja B koos toimumise tõenäosus ehk **korrutise tõenäosus**
on võrdne ühe osasündmuse tõenäosuse ja teise osasündmuse tingliku tõenäosuse korrutisega:

    :math:`P(AB) = P(A) \times P(B | A) = P(B) \times P(A | B)`

**Kehtib siis, kui A ja B on sõltuvad**.

**Kui sündmused A ja B on sõltumatud**, siis :math:`P(B | A) = P(B)`
ja tõenäosuste korrutamislausest järeldub, et

    :math:`P(AB) = P(A) \times P(B)`

ehk sõltumatute sündmuste korrutise tõenäosus on võrdne osasündmuste tõenäosuste korrutisega.

Korrutamisteoreemi üldistus
---------------------------
.. image:: /studies/matemaatika/statistiline_analüüs/pildid/korrutamisteoreemi_üldistus.PNG
   :width: 300pt
   :align: center

Esimese puhul eeldad kõikide varasemate sündmuste toimumist.
Kui on sõltumatud, siis nende korrutise tõenäosus on sama hea mis tõenäosuste korrutis.

Näide sõltuvatest sündmustest:

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/KT_näide_sõltuv.PNG
   :width: 300pt
   :align: center

Näide sõltumatutest sündmustest:

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/KT_tankinäide.PNG
   :width: 300pt
   :align: center

Näide kui sündmusi on palju:

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/KT_lotonäide.PNG
   :width: 300pt
   :align: center

Kombinatoorika
--------------
Faktoriaal
^^^^^^^^^^
Arvu faktoriaali tähistatakse nii, et arvu järgi pannakse hüüumärk. Tähis on n!
Faktoriaal on vastavate järjestikuste naturaalarvude korrutis.

    :math:`n \times n(-1) \times (n-1) \times ... \times 3 \times 2 \times 1`

Eraldi on kaks faktoriaali: :math:`1! = 1 \text{ ja } 0! = 1`. See, et nulli faktoriaal on üks, on kokkuleppeline!!

Näide: NB! Faktoriaal on defineeritud vaid naturaalarvude jaoks!

    :math:`2,5!` ei saa arvutada, sest ta pole naturaalarv.

Permutatsioonid
^^^^^^^^^^^^^^^
Permutatsioonide arv n elemendist on arv, mis näitab mitu erinevat järjekorda saab moodustada n elemendist.

Permutatsioonide arvu tähis on :math:`P_n`

Arvutusvalem:

    :math:`P_n = n! = 1 \times 2 \times 3 \times ... \times n`

Kombinatsioonide arv
^^^^^^^^^^^^^^^^^^^^
Kombinatsioonide arv n elemendist k kaupa näitab, mitu erinevat võimalust on valida k elementi n elemendi hulgast.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/kombinatsioonid.PNG
   :width: 300pt
   :align: center

Kombinatoorika põhireeglid
^^^^^^^^^^^^^^^^^^^^^^^^^^
Kombinatoorikal on kaks põhireeglit - liitmise reegel ja korrutamise reegel.

Liitmise reegel - kui mingi elemendi A võib valida k erineval viisil, elementi B aga l erineval viisil,
siis elemendi **"kas A või B"** võib valida k + l erineval viisil.

Korrutamise reegel - kui mingi elemendi A võib valida k erineval viisil, elementi B aga l erineval viisil, siis elemendi
**"A ja B"** saab valida k * l erineval viisil.

Bernoulli valem
---------------
NB! Kehtib sõltumatute sündmuste katseseeria puhul.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/Bernoulli_valem.PNG
   :width: 300pt
   :align: center

Korrutised erinevad vaid tegurite järjekorrast.
Seetõttu piisab, kui leiame ühe korrutise väärtuse ja korrutame skeemi ridade arvuga.

Kas meil on vahend, mis ütleb mitu rida sellises skeemis on? Siin tuleb appi kombinatsioonide arv - neljast kahekaupa tükki.
Tulemus korrutatakse ühe skeemirea väärtusega.

Sõnadega: kombinatsioonide arv kogu katseseeria pikkusest meid huvitava arvu kaupa korda meid huvitava üksiksündmuse tõenäosus astmel meid huvitav arv,
korda meid huvitava sündmuse vastandsündmuse toimumise tõenäosus astmes kogu katseseeria pikkus miinus meid huvitav arv.

Bernoulli valemi tuletamine
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Kuna teame, et sündmused A on sõltumatud ja ühesugused, siis sellise korrutise puhul lagunevad korrutiste tõenäosused
tõenäosuste korrutiseks. P(AB) = P(A) * P(B).

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/Bernoulli_valemi_tuletamine.PNG
   :width: 300pt
   :align: center

Bernoulli valemil on ka erijuhud:

    -   kui k = n, siis :math:`P_{n,n} = p^n`, sest vahe väärtuseks tuleb 0 ehk q on 1 ning kombinatsioonide arv tuleb ka 1, sest k = n korral on tulemus 1.

    -   kui k = 0, siis :math:`P_{n,0} = q^n` on selgitus analoogne.

Juhuslik sündmus vs juhuslik suurus
-----------------------------------
Juhuslik sündmus väljendab millegi toimumist või mittetoimumist.

Suurust nimetatakse juhuslikuks, kui see omandab antud
tingimustes sõltuvalt juhusest ühe oma võimalikest väärtustest.

Juhuslik suurus on matemaatilises mõttes muutuja, millel on palju erinevaid väärtusi.

Nt. täringuviske tulemus (täringuviskel saadavate silmade arv)

NB! Juhuslik suurus peab olema sõnastatud nii, et tal on palju erinevaid väärtusi. Kui panna arv sõnastusse,
siis see aitab tagada, et väärtusi on palju.

Diskreetsed ja pidevad juhuslikud sündmused
-------------------------------------------
Juhuslikke suurusi tähistatakse ladina suurtähtedega X, Y, ... ja
juhuliku suuruse võimalikke väärtusi indeksitega väiketähtede
abil: :math:`x_1, x_2, ..., y_1, y_2, ...`

Meenutame, et juhuslikke sündmusi tähistame tähestiku algusosa suurtähtedega. Suurusi aga lõpuosa suurtähtedega.

**Juhusliku suuruse mingi väärtuse esinemine on juhuslik sündmus.**

Juhuslikud suurused jaotuvad kahte klassi: diskreetseteks ja
pidevateks.

**Diskreetseks** nimetatakse juhuslikku suurust, mille võimalike
väärtuste hulk on lõplik või loenduv (nummerdatav).
Näiteks: Laskude arv märklaua tabamiseni.

Juhuslikku suurust nimetatakse **pidevaks**, kui tema võimalike
väärtuste hulk on arvtelje (lõplik või lõpmatu) vahemik.
Näiteks: Vooluvõrgu pinge mõõtmise tulemus.

Diskreetse juhusliku suuruse jaotusseadus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Tõenäosus, et juhuslik suurus X saavutab ühe oma võimalikest
väärtustest:

    :math:`P(X = x_i) = p_i`

Diskreetse juhusliku suuruse jaotusseaduseks nimetatakse
vastavust tema kõikide võimalike väärtuste :math:`x_1, x_2, ... \text{ ja nende tõenäosuste } p_1, p_2, ...` vahel.

Üks võimalus on esitada jaotusseadus **jaotustabelina** ehk **jaotusreana**.

Jaotustabel sisaldab juhusliku suuruse kõiki võimalike väärtusi ja seetõttu

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/jaotustabel.PNG
   :width: 300pt
   :align: center

Juhul kui on teada kõik võimalikud tulemused ehk väärtusedm ehk on täielik süsteem,
siis on teada, et täieliku süsteemi tõenäosuste summa on 1.

Jaotushulknurk ehk jaotuspolügoon
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Diskreetset juhuslikku suurust esitatakse ka graafiliselt jaotushulknurgana e. jaotuspolügoonina,
kus arvupaaridele (xi , pi) vastavad punktid ristkoordinaadistikus on ühendatud sirglõikudega.

Joonestamiseks võtame punktipaarid ja paneme graafikule.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/jaotushulknurk.PNG
   :width: 300pt
   :align: center

Juhusliku suuruse jaotusfunktsioon
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Juhusliku suuruse jaotusfunktsioon F(x) määrab tõenäosuse
selleks, et juhuslik suurus on väiksem tõkkest x , s. t.

:math:`F(x) = P(X < x), \text{ kus } x \incl (-\infty, \infty)`

Olgu juhuslikuks suuruseks X juhuslik punkt, mis katse
tulemusena satub x-teljele.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/juhuslikus_suuruse_jaotusfunktsioon.PNG
   :width: 300pt
   :align: center

Jaotusfunktsioon määrab iga x puhul tõenäosuse, et juhuslik punkt
X asetseb punktist x vasakul.

Näide:

NB! Vaata alati tabeli esimest väärtust. See pole alati 0!

Kuigi tõke võib olla 0-s, siis definitsiooni järgi vaatame kas väärtused on väiksemad 0-st ehk väiksemad tõkkest.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/juhuslik_suurus_naide.PNG
   :width: 300pt
   :align: center

*Järeldus* Diskreetse juhusliku suuruse jaotusfunktsioon võrdub argumendist rangelt väiksemate väärtuste xi
tõenäosuste summaga:

:math:`F(x) = \lim_{x_i < x} P(X = X_i)`

Tulemused tuleks ka kokku võtta.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/jaotusfunktsiooni_kokkuvõte.PNG
   :width: 300pt
   :align: center

Diskreetse suuruse puhul on tähtis jälgida, mis tuleb kaasa ja mis ei tule.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/jaotusfunktsiooni_tulemus.PNG
   :width: 300pt
   :align: center

Kui on vaja diskreetse juhusliku suuruse tõenäosust, siis selle annab jaotusfunktsiooni väärtuste vahe.

:math:`P(a \leq x < b) = F(b) - F(a)`

Jaotusfunktsiooni omadused
^^^^^^^^^^^^^^^^^^^^^^^^^^
1. JF-i väärtus kohal miinus lõpmatus on 0 ehk :math:`\lim_{x \rightarrow -\infty} = 0` ehk :math:`F(-\infty) = 0`

2. JF-i väärtus kohal pluss lõpmatus on 1 :math:`\lim_{x \rightarrow \infty} = 1` ehk :math:`F(-\infty) = 1`

3. JF on mittekahanev ehk ta võib kasvada ja sisaldada konstantsuse piirkondi ehk monotoonselt kasvav. S.t kui
:math:`x_2 \geq x_1`, siis :math:`F(x_2) \geq F(x_1)`

4. Pideva juhusliku suuruse JF on pidev.

Pidev juhuslik suurus
---------------------
Pideva juhusliku suuruse eriomadus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: /studies/matemaatika/statistiline_analüüs/pildid/pideva_juhusliku_suuruse_eriomadus.PNG
   :width: 300pt
   :align: center

Miks on pideva juhusliku suuruse korral suuruse mistahes üksikväärtuse esinemise tõenäosus null?

Sest kui on diskreetne suurus ja olen fikseerinud punkti, siis delta langeb hüppele, mitte punktile.

Pideva juhusliku suuruse jaotusfunktsioon
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: /studies/matemaatika/statistiline_analüüs/pildid/pideva_juhusliku_suuruse_jf.PNG
   :width: 300pt
   :align: center

Kui meid pideva suuruse puhul huvitab a-st b-ni sattumise tõenäosus, siis pole vahet, kas satume
poollõiku sattumise või vahemikku sattumise tõenäosusest. Kuna üksikväärtusesse jõudmise tõenäosus
on niikuinii null ehk teda me ei arvesta, siis on tõenäosus võrdne JF-i juurdekasvuga ehk JF väärtuste vahega.

Pideva väärtuse puhul üksikväärtust ei arvestata.

Tõenäosuse tihedus e. tihedusfunktsioon
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: /studies/matemaatika/statistiline_analüüs/pildid/tihedusfunktsioon.PNG
   :width: 300pt
   :align: center

NB! Tihedusfunktsioon on olemas vaid pidevatel juhuslikel suurustel!

Tihedusfunktsiooni omadused
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Omadusi on kaks:

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/TF_omadused.PNG
   :width: 300pt
   :align: center

Esimese järgi on tihedusfunktsiooni väärtused alati ülalpool x-telge.
Teise järgi on graafikualuse ala pindala alati 1.

Iga funktsioon, mis täidab 1. ja 2. punkti, on mingi funktsiooni tihedusfunktsioon.
Lihtsamas mõttes võib TF-i defineerida nende kahe omaduse kaudu.

Tihedusfunktsiooni graafik
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: /studies/matemaatika/statistiline_analüüs/pildid/TF_graafik.PNG
   :width: 300pt
   :align: center

Juhusliku suuruse antud poollõiku sattumise tõenäosus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: /studies/matemaatika/statistiline_analüüs/pildid/poollõik.PNG
   :width: 300pt
   :align: center

Oluline on kinni püüda põhilised tulemused. - saame leida jaotusfunktsiooni ja tihedusfunktsiooni abil.
Ühe ja sama tõenäosuse leidmiseks on kaks varianti!!! Annab painduvust.

Arvkarakteristikud
------------------
Kasutame, kui tahame teada juhusliku suuruse mõningaid omadusi.
Näiteks mingis mõttes keskmist väärtust väljendavad arvud.

Väärtuste keskmistest tasemest räägib **juhusliku suuruse keskväärtus**.
Tema sisuline vaste on aritmeetiline keskmine.

Juhusliku suuruse keskväärtus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
**Diskreetse** juhusliku suuruse X keskväärtuseks **EX**
nimetatakse suuruse võimalike väärtuste ja nende tõenäosuste korrutiste summat.

    :math:`EX = \sum_{i=1}^{n} x_i p_i`

**Pideva** juhusliku suuruse X keskväärtus on:

    :math:`EX = \int_{-\infty}^{\infty} xp(x)dx`

Arvuline näide:

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/JS_keskväärtus_näide.PNG
   :width: 300pt
   :align: center

Juhusliku suuruse keskmine lineaarhälve
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: /studies/matemaatika/statistiline_analüüs/pildid/keskmine_lineaarhälve.PNG
   :width: 300pt
   :align: center

Juhusliku suuruse dispersioon
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Dispersiooniga hindame väärtuse kaugust tsentrist.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/juhuslikus_suuruse_dispersioon.PNG
   :width: 300pt
   :align: center

Näide:

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/dispersioon_standardhälve_näide.PNG
   :width: 300pt
   :align: center

Näide 2:

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/näide2.PNG
   :width: 300pt
   :align: center

Klassikalised jaotused
----------------------
Klassikalised ehk mudeljaotused on teatud kindlate omadustega
juhuslike suuruste jaoks kirjutatud valemikomplektid.

Neid on nii diskreetsete kui ka pidevate suuruste jaoks.

Diskreetse puhul on olulisim binoomjaotus, pidevate puhul normaaljaotus.

Binoomjaotus
^^^^^^^^^^^^
Binomiaalne juhuslik suurus tekib **sõltumatute katsete** korral,
kus juhuslikuks suuruseks on meid huvitava **sündmuste toimumiste arv**.
Näiteks märki tabanud laskude arv korduval tulistamisel.

Tõenäosused :math:`P_{n, m}` üldiselt kasvavad maksimaalse väärtuseni
ja siis jälle kahanevad.

Katsete arvu n suurenemisel sama p korral tõenäosuse :math:`P_{n, m}`
väärtused vähenevad.

Kui p = q, siis jaotus on sümmeetriline iga n korral. Kui p ei võrdu q,
siis ebasümmeetriline. Aga katsete arvu n suurenemisel läheneb jaotus
sümmeetrilisele isegi teineteisest erinevate p ja q korral.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/binomiaalne_juhuslik_suurus.PNG
   :width: 300pt
   :align: center

Näide:

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/binoomjaotus_näide.PNG
   :width: 300pt
   :align: center

Binoomjaotuse parameetrid
^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/binoomjaotuse_parameetrid.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/binoom_kirjutis.PNG
   :width: 300pt
   :align: center

Normaaljaotus
^^^^^^^^^^^^^
Suur osa loodust ja ühiskonda kujutavatest juhuslikest suurustest
allub normaaljaotusele.

Normaaljaotus tekib järgmistel tingimustel:

    -   tunnuse väärtus kujuneb paljude üksteisest sõltumatute nõrgalt mõjuvate faktorite toimel.
    -   tunnuse väärtustel on olemas mingi fikseeritud keskmine tase.
    -   tunnuse väärtuste suurenemine üle keskmise taseme ja vähenemine alla keskmist taset on võrdvõimalikud.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/normaaljaotus.PNG
   :width: 300pt
   :align: center

N tähistab siin keskväärtust, sigma standardhälvet.
Kui :math:`m = 0` ja :math:`\sigma = 1`, siis nimetatakse vastavat
normaaljaotust *normeerituks*.

Normaaljaotuse parameetrid
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/normaaljaotuse_parameetrid.PNG
   :width: 300pt
   :align: center

Vahemikku sattumise tõenäosus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Kasutatakse siis, kui meil on normaaljaotusega juhuslik suurus
ja tahame leida piirkonda sattumise tõenäosust,
siis sobib selleks Laplace'i valem.

Asendus toimub muutujavahetusega. Leian diferentsiaali avaldise.
Funktsioon fii.

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/laplace.PNG
   :width: 300pt
   :align: center

Laplace'i funktsiooni omadusi
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/laplace_omadused.PNG
   :width: 300pt
   :align: center

Näide 1

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/laplace_näide.PNG
   :width: 300pt
   :align: center

Tsentreeritud hälve ehk erinevus keskmisest
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/näide3.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/näide3_2.PNG
   :width: 300pt
   :align: center

Binoomjaotuse koondumine normaaljaotuseks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/BJ_koondumine_NJ.PNG
   :width: 300pt
   :align: center

Moivre-Laplace'i integraalne piirteoreem
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/ML1.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/ML2.PNG
   :width: 300pt
   :align: center

Näide:

.. image:: /studies/matemaatika/statistiline_analüüs/pildid/ML_näide.PNG
   :width: 300pt
   :align: center

