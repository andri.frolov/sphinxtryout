Matemaatiline analüüs
#####################
Sissejuhatus
============
Matemaatiline analüüs on matemaatika haru mis hõlmab diferentsiaal ja integraalarvutust.
Diferentsiaalarvutus tähendab kõike seda, mis on seotud tuletistega.
Integraalarvutus tähendab seda, mis on seotud integraalidega.

Matem analüüs jaguneb kaheks: tuletistega ja integraalidega seotud temaatikaks.

Mõlemad on tehted, mida tehakse funktsioonidega ja mida defineeritakse piirväärtuste abil.

Selle mooduli põhiteemad on: **funktsioon, piirväärtus, pidevus, tuletis, integraal**.

Funktsioon ja temaga seotud mõisted
===================================
Olgu X mingi reaalarvude hulk. Kui muutuja x igale väärtusele hulgas X vastab muutuja y **üks kindel väärtus**, siis öeldakse, et y on muutuja x funktsioon.
Asjaolu, et üks muutuja on teise funktsioon, tähistatakse y = f(x), y = y(x), y = (x) jne.

**Sõltumatu muutuja** e. **argument** on siin x. On ka kutsutud x-i põhjuseks ja y-t tagajärjeks.

**Sõltuv muutuja** on see, mille väärtused leitakse vastavalt sõltumatu muutuja väärtustele. Siin on sõltuv muutuja y.

**Määramispiirkond** on argumendi x väärtuste hulk, mille puhul saab määrata funktsiooni y väärtusi vastavalt eeskirjale f(x).
Tähis on suur X ehk argumendi suurem variant.

**Muutumispiirkond** on funktsiooni määramispiirkonnale vastavate väärtuste hulk.

Jäta meelde nii, et muutumispiirkonna tähis tuleb alati võrdusmärgi vasakult ja määramispiirkonna tähis paremalt.
Jäta meelde, et U on enne Ä-d.

Pöördfunktsioon
===============
Olgu funktsiooni y = f(x) määramispiirkond X ja muutumispiirkond Y.

Kui iga korral leidub täpselt üks, nii et y = f(x), siis öeldakse, et funktsioonil y = f(x) on olemas pöördfunktsioon määramispiirkonnaga Y ja muutumispiirkonnaga X.

Funktsiooni pöördfunktsiooni pöördfunktsioon on funktsioon ise.
Pöördfunktsiooni tähistatakse sageli x = f-1(y). Argument on avaldatud funktsiooni tähise kaudu.
Meie ei kasuta, sest meil pole kõiki tähiseid tarvis.

NB! Pöördfunktsiooni ei pruugi alati olemas olla. On ainult siis, kui y korral vastab talle ainult üks x.
Ehk sama tingimus mis funktsiooni korral, ainult pööratuna.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/poordfunktsioon.PNG
   :width: 300pt
   :align: center

NB! Gümnas õppisime pöördfunktsiooni esitama nii, et funktsioon on alati y ja argument x-iga ja ainult seepärast vahetame pooled.
Matemanis vahetatakse harvem. Nt siis, kui esitada funk ja pöördfunk samas koordinaadistikus. (vt pilti)

Pöördfunk on funkiga alati sümmeetriline sellise sirge suhtes, mis poolitab 1. ja 3. veerandit ehk y = x. (pildil must).
Sellega saab konstrueerida pöördfunktsiooni ja vastupidi.

Kas ruutfunktsioonil on pöördfunktsioon?

Üldiselt ei, kuid saamiseks kitsendame ehk ahendame määramispiirkonda.
Kui esialgne funk mäpi on miinus lõpmatusest lõpmatuseni, siis pärast kitsendamist on nullist lõpmatuseni.
Selle tulemusena jääb järgi paraboolist vaid üks haru. Ja nüüd on vastab igale y-le täpselt üks x.
Mäpi kitsendamine on nipp pöördfunktsiooni leidmiseks.
NB! Matemaatikud kasutavad terminit ahendamine, tavakeeles kitsendamine.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/ahendamine.PNG
    :width: 300pt
    :align: center

Näide 1
-------

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/naide1.PNG
    :width: 200pt
    :align: center

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/naide1ahendamine.PNG
    :width: 300pt
    :align: center

Siinusfunktsiooni pöördfunktsioon on arkussinus. Pildil on näidatud nende sümmeetria.
Miks sellises piirkonnas lõik? Võin ju võtta ükskõik kust.
Puhas kokkulepe! Samad kokkulepped on ka kõikides teistes trigonomeetrilistes funktsioonides.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/arkussiinus.PNG
    :width: 300pt
    :align: center

Peab teadma, et pöördfunktsiooni loomisel saab algse funktsiooni muutumispiirkonnast pöördfunktsiooni määramispiirkond ja määramispiirkonnast muutumispiirkond.
Määramispiirkond ei pea olema alati x-iga. Mäpi peab olema tähistatud sama tähega millega on tähistatud argument!
Kuna hetkel on argument y, siis tuleb mäpit tähistada ka y-ga.
Kui vaadata jooniselt arcsin ülesehitust, siis funktsiooni väärtusi tähistatakse väärtusega x ja seetõttu on tema muutumispiirkond suur X.
Muutumispiirkond on alati väärtuste hulk!

**Pöördfunktsiooni määramis- ja muutumispiirkond kirjutatakse alati välja esialgse funktsiooni põhjal!**

Miks me ei saa võtta pöördfunktsiooni määramis- ja muutumispiirkonda pöördfunktsiooni pinnalt?
----------------------------------------------------------------------------------------------
Üldjuhul pöördfunktsiooni avaldisest ei saa tema mäpit ja mupit leida selle pärast,
et meil pole õigust vaadata seda funktsiooni iseseisvana.
See funktsioon on leiutatud millestki - algfunktsioonist - ja kõik selle esialgse funktsiooni omadused tulevad kaasa!

Kuidas tekivad trigonomeetrilistele funktsioonidele pöördfunktsioonid?
----------------------------------------------------------------------
Siinus ja arkussiinus
^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/sin_arcsin.PNG
   :width: 300pt
   :align: center

Koosinus ja arkuskoosinus
^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/cos_arccos.PNG
   :width: 300pt
   :align: center

Tangens ja arkustangens
^^^^^^^^^^^^^^^^^^^^^^^
Aluseks võetakse tangensi põhiharu.
Vahemik (sulud) seepärast, et tangensi harud on asümptootilised - nad lähenevad püstsirgele pii-kahendik, kuid ei lähe talle kunagi pihta.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/tan_arctan.PNG
   :width: 300pt
   :align: center

Kotangens ja arkuskotangens
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Pöördfunki konstrueerimiseks kasutatakse samuti põhiharu.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/cotan_arccotan.PNG
   :width: 300pt
   :align: center

Näide 2
-------
Näide pöördfunktsioonide kasust pärismaailmas, antud juhul füüsikas.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/naide2.PNG
   :width: 300pt
   :align: center

Näide 3
-------
Näide logaritmfunktsioonist. Ülesandes kümnendlogaritm, mille puhul logaritmi alust ei märgita.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/logaritmfunk.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/naide3.PNG
   :width: 300pt
   :align: center

Pöördfunktsiooni jaoks tuleb leida funktsioon argumendi järgi.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/logaritmipoordfunk.PNG
   :width: 300pt
   :align: center

Liitfunktsioon
==============
Liitfunktsioon tekib siis, kui funktsioon ei sõltu oma argumendist otse, vaid kaudse, vahendava muutuja kaudu.

Liitfunktsiooni koostisosad ehk komponendid, sisemine ja välimine funktsioon
----------------------------------------------------------------------------

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/liitfunkdefinitsioon.PNG
   :width: 300pt
   :align: center

Hea meelde jätta matrjoška analoogiga - funktsioonid on nagu matrjoška, üks teise sees.
Kui märkad, et funktsioonid on nii kokku pandud, siis on silme ees liitfunktsioon.

Seda oskust on vaja tuletise määramisel.

Näited: ruutfunktsioon

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/liitfunknaide.PNG
   :width: 300pt
   :align: center

Elementaarsed põhifunktsioonid
==============================
Elementaarsed põhifunktsioonid on enim uuritud ja enim kasutatud funktsioonid.

**Peab peast teadma nende määramis- ja muutumispiirkondi ning oskama joonestada nende graafikuid.**

Materjalide hulgas on leht, kuhu saab kõikide funktsioonide põhilised omadused välja joonistada.

Sinna kuuluvad:

    -   **Konstantne funktsioon**: :math:`y = c`

        -   Graafikud on kõik paralleelsed x teljega.

    -   **Astmefunktsioon**, olenemata astendajast: :math:`y = x^a`, kus :math:`a` on reaalarv.

        -   Neid saab jagada alamjuhtudeks ehk funktsiooni omadused olenevalt astendajast. Meie neid kursusel ei vaata.

    -   **Eksponentfunktsioon**: :math:`y = a^x`, kus :math:`a` on ühest erinev positiivne arv.

        -   Kindlasti peab oskama mõelda, et miks a peab olema ühest erinev. Kui a = 1, siis tulemus on 1.

            Sinna tekib konstantne funktsioon, mis on kaetud esimese juhtumiga. Seepärast arvatakse 1 välja.
            Seda, et a ei tohi olla 1, ei tooda sageli välja. Peetakse elementaarseks.

    -   **Logaritmfunktsioon** ehk eksponentfunktsiooni pöördfunktsioon: :math:`y = log _ a x`, kus alus :math:`a` on ühest erinev positiivne arv.

        -   Kuna eksponentfunktsiooni puhul pole :math:`a = 1` lubatud, siis pole ka logaritmfunktsioonis, sest pöördfunktsioon saab kaasa algfunktsiooni omadused.

        -   **NB! Logaritmi alus ei tohi olla 1 ja seda läheb ülesannetes vaja!**

    -   **Trigonomeetrilised funktsioonid**

        -   :math:`y = sin x`, :math:`y = cos x`, :math:`y = tan x`, :math:`y = cot x`

    -   **Arkusfunktsioonid**

        -   :math:`y = arcsin x`, :math:`y = arccos x`, :math:`y = arctan x`, :math:`y = arccot x`

Eksponentfunktsioon :math:`y = a^x`
-----------------------------------
Graafik sõltub sellest, kas alus on ühest suurem või nulli ja ühe vahel.

Kaks võimalikku haru.
Kui :math:`a > 1`, siis on **kasvav funktsioon**, kui :math:`0 < a < 1`, siis kahanev.

NB! Kõik eksponentfunktsioonid läbivad y graafikul punkti 1.
NB2! Alus ei tohi olla 1.

-   Määramispiirkond on :math:`X = (-\infty ; \infty)`.
-   Muutumispiirkond on :math:`Y = (0 ; \infty)`.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/eksponentfunknaide.PNG
   :width: 300pt
   :align: center

Logaritmfunktsioon :math:`y = log _ a x`
----------------------------------------
Oluline teada, et olenemata logaritmi alusest lähevad graafikud läbi punktist 1.

Samuti on võimalikke harusid kaks, olenevalt sellest, kas :math:`a > 1` või :math:`0 < a < 1`.

-   Määramispiirkond on :math:`X = (0 ; \infty)`.
-   Muutumispiirkond on :math:`Y = (-\infty ; \infty)`.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/logaritmfunknaide.PNG
   :width: 300pt
   :align: center

Trig. funktsioonid siinus ja koosinus
-------------------------------------
Siinusfunktsioon läheb kindlasti läbi 0-punktist (x = 0 ja y = 0). Pikkus -pii kahendikku kuni pii kahendikku.

-   Määramispiirkond on :math:`X = (-\infty ; \infty)`.
-   Muutumispiirkond on :math:`Y = [-1 ; 1]`.

NB! Siinusfunktsioon on **paaritu funktsioon**.

Koosinusfunktsioon läbib punkti x = 0, y = 1. Kordumatu lõigu pikkus -pii kuni pii.

-   Määramispiirkond on :math:`X = (-\infty ; \infty)`.
-   Muutumispiirkond on :math:`Y = [-1 ; 1]`.

NB! Koosinusfunktsioon on **paarisfunktsioon**.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/trigfunk.PNG
   :width: 300pt
   :align: center

Trig. funktsioonid tangens ja kotangens
---------------------------------------
**Tangens** koosneb mitmest harust. Tal on püstsirged, millega ta iial kokku ei saa.
Püstsirgete kohad on need, mis jäävad MäPist välja.

-   Määramispiirkond on :math:`X = (-\infty ; \infty)` \\ :math:`{(2k + 1) \pi/2}`

    -   Tähendab seda, et :math:`2k + 1` on paaritu arv. Üldine kirjutusviis.

-   Muutumispiirkond on :math:`Y = (-\infty ; \infty)`

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/trigfunktangens.PNG
   :width: 300pt
   :align: center

**Kotangens** koosneb samuti paljudest harudest.

-   Määramispiirkond on :math:`X = (-\infty ; \infty)` \\ :math:`{k\pi}`

    -   Välja jäävad reaalarvud, mis pole arvu :math:`\p` täisarvkordsed.

-   Muutumispiirkond on :math:`Y = (-\infty ; \infty)`

NB! Kotangens on **paaritu funktsioon**.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/trigfunkkotangens.PNG
   :width: 300pt
   :align: center

Arkusfunktsioonid
-----------------
Slaidi peal on tähised sedasi, et sõltuv muutuja on y ja argument on x.
Oluline teada, et MUPI on siin Y ja MÄPI on X. Tähed sõltuvad sellest, kuidas oleme funktsiooni lahti kirjutanud.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/arkusfunktsioonid.PNG
   :width: 300pt
   :align: center

arctan ja arccot puhul on tähtis välja näidata :math:`\pi/2`. See :math:`\pi/4` pole ilmtingimata tähtis.

Elementaarfunktsiooni mõiste
============================
Elementaarfunktsiooniks nimetatakse funktsiooni, mis saadakse põhielementaarfunktsioonidest lõpliku arvu
aritmeetiliste tehete ja liitfunktsioonide moodustamise tulemusena.

Elementaarfunktsioon võib kõlada lihtsalt, kuid ei pruugi. PEF-ist saab ehitada väga keerulisi funktsioone.

1.  Kaks lineaarfunktsiooni, mida ühendab jagamistehe. See moodustab sisemise funktsiooni ja välimine on astmefunktsioon astendajaga 2.

2.  Lahutamistehe ühendab konstantset funktsiooni trigonomeetrilise funktsiooniga. Lisaks kasutatud liitfunktsiooni moodustamist ehk astendamine kolmandikuga.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/elementaarfunk.PNG
   :width: 300pt
   :align: center

Reaalarvu absoluutväärtus
=========================
Reaalarvu x absoluutväärtuseks ehk mooduliks, tähistatakse :math:`|x|`, nimetatakse mittenegatiivset reaalarvu,
mis rahuldab tingimusi

-   :math:`|x| = x`, kui x on suurem-võrdne 0-iga.

-   :math:`|x| = -x`, kui x on väiksem  0-iga.

NB! Esimesel puhul tapab negatiivsuse!

Absoluutväärtuse geomeetriline tõlgendus
----------------------------------------
Geomeetriliselt tõlgendades tähendab absoluutväärtus seda arvu arvteljel kujutava punkti kaugust nullpunktist.

Näited:
^^^^^^^
:math:`|7| = 7`, sest ta asub nullpunktist 7-e ühiku kaugusel.
:math:`|-7| = -(-7) = 7`, sest ka -7 asub nullpunktist 7 ühiku kaugusel.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/absoluutvaartus.PNG
   :width: 300pt
   :align: center

Absoluutväärtuse omadusi
------------------------
-   Absoluutväärtus on mittenegatiivne. Ta ei saa olla negatiivne, nagu kaugus.

    -   :math:`|x| \geq 0`

-   Absoluutväärtus negatiivsest arvust on sama mis positiivsest arvust.

    -   :math:`|-x| = |x|`

-   Absoluutväärtuse märkidega või korrutamise ja jagamise puhul minna liikmete ümber.

    -   :math:`|x * y| = |x| * |y|`

    -   :math:`|x / y| = |x| / |y|`

-   Liitmine ja lahutamine, MEIE SEDA EI KASUTA!! Siin vaid vältimaks olukorda,
    et kasutad liitmisel ja lahutamisel sama loogikat mis korrutamisel-jagamisel.

    -   :math:`|x + y| <= |x| + |y|`

    -   :math:`|x - y| \geq |x| - |y|`

Absoluutväärtuste definitsioonist järelduv!!
--------------------------------------------
-   :math:`|x| = a <=> x = a või x = -a`

    -   :math:`|x| = 3 <=> x = 3 või x = -3`

    -   Meid huvitavad punktid, mille kaugus nullist on 3. Saad 3 ja -3.

    -   :math:`|x - 2| = 8 <=> x - 2 = 8` või :math:`x - 2 = -8`

-   :math:`|x| < a <=> -a < x < a`

    -   :math:`|x| < 3 <=> -3 < x < 3`

    -   Meid huvitavad punktid, mille kaugus nullpunktist on vähem kui 3 ühikut.

    -   :math:`|x - 3| < 5 <=> -5 < x - 3 < 5`

    -   Selliseid ahelvõrratusi võib lahendada ühe tükina,
        mis annab tunduvalt lühema lahendustee kui keskkoolis õpitud kahe lahendusega variant.

        -   Selleks loo olukord, kus keskel on vaid tundmatu.
            Antud juhul paned kolme juurde igale liikmele

        -   :math:`-5 + 3 < x - 3 + 3 < 5 + 3 <=> -2 < x < 8`

        -   Vastuseks sobib ka x € (-2; 8)

-   :math:`|x| > a <=> x > a` või :math:`x < -a`

    -   :math:`|x| > 3 <=> x > 3` või :math:`x < -3`

    -   Meid huvitavad punktid, mille kaugus nullpunktist on rohkem kui 3.

    -   :math:`|3x - 7| > 5 <=> 3x - 7 > 2` või :math:`3x - 7 < -2`

    -   :math:`x > 3` ja :math:`x < 5/3`

Soovitus - ära karda panna valemisse arve ja üritada tulemust lahti mõtestada.
Pane end konkreetsesse olukorda.

Ruutvõrratus
============

:math:`x^2 - 2x - 15 \geq 0`

-   Kõigepealt lahendame vastava võrrandi. Selleks on kaks valemit, meile satub sageli esimene, sest muutuja kordaja on sageli 1.

    -   .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/ruutvorrand.PNG
           :width: 200pt
           :align: center

    -   :math:`x _ 1 = 5` , :math:`x _ 2 = -3`

-   Skitseerime parabooli

    -   Antud juhul teame, et muutuja ees on plussmärk.
        See tähendab, et parabool avaneb ülespoole ehk algab paremalt ülevalt.

    -   Kuna võrratus on suurem-võrdne märgiga, siis tuleb punktid piirkonna sisse arvata.

    -   x € (-00; -3] U [5; 00)

Mis siis, kui võrratuses on muutuja ees miinusmärk?

:math:`-x^2 + 2x + 15 \geq 0`

Kõigepealt tuleks miinus :math:`-x^2` eest ära hävitada ehk korrutada :math:`-1`-ga.
*NB! Siis muutub võrratuse märk vastupidiseks*

:math:`x^2 - 2x - 15 \leq 0`

Kuidas joonestada parabooli?
*Kuna tegime algülesandele teisendusega uue kuju, siis kogu arendus peab olema kooskõlas uue kujuga, sest peame algülesannet ebamugavaks.*

*Kehtib reegel, et kui tegid algvõrratusest teisenduse, siis sinu lahendus peab olema kooskõlas teisendusega, mitte algvõrratusega!*

*Veaohtlik koht!* See parabool avaneb üles!! Vastused loeme allpool x-teljest, sest väiksem-võrdne nullist.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/ruutvorratusparabool.PNG
   :width: 200pt
   :align: center

Paaris- ja paaritu funktsioon
=============================
Funktsioon :math:`y = f(x)` on

    -   **paaris**, kui :math:`f(-x) = f(x)`

    -   **paaritu**, kui :math:`f(-x) = -f(x)`

iga x-i korral määramispiirkonnast X.

Kontroll käib nende tingimuste täitmise järgi. Funktsioon võib olla ka ei paaris ega paaritu.

-   **Paarisfunktsiooni graafik on sümmeetriline y-telje suhtes.**

-   **Paaritu funktsiooni graafik on sümmeetriline 0-punkti suhtes.**

Hea teada, et:

    -   **ainuke trigonomeetriline paarisfunktsioon on koosinus. Ülejäänud on paaritud**.

    -   Kõik paarisarvulise astendajaga funktsioonid on paarisfunktsioonid, paarituarvulise astendajaga on paaritud funktsioonid.

Piirväärtus
============
Abimõiste: punkti ümbrus
------------------------
Punkti :math:`a` ümbruseks nimetatakse suvalist vahemikku, millesse see
punkt kuulub. Matanalüüsis räägitakse punkti epsilon ümbrusest.

Punkti :math:`a` ümbruseks raadiusega :math:`{\epsilon} > 0`, nimetatakse arvtelje
vahemikku arvust :math:`a - {\epsilon}` kuni :math:`a + {\epsilon}`.

Öeldakse punkti epsilon ümbrus või punkti ümbrusraadiusega epsilon.
Miks raadiusega? Kui konstrueerida ka ringjoon raadiusega epsilon,
siis saame sama ala.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/delta.PNG
   :width: 300pt
   :align: center

Arv x kuulub arvu a ümbrusesse raadiusega :math:`{\epsilon}`, kui:

-   :math:`a - {\epsilon} < x < a + {\epsilon}`
-   :math:`-{\epsilon} < x - a < {\epsilon}`
-   :math:`|x - a| < {\epsilon}`

Viimase kohta - arv x kuulub arvu a ümbrusesse raadiusega epsilon siis,
kui x-i ja a vaheline kaugus on väiksem kui epsilon.

Reaalarvu a **parempoolseks ümbruseks**, täpsemalt :math:`{\epsilon}` ümbruseks,
nimetatakse arvtelje vahemikku arvust :math:`a` kuni :math:`a + {\epsilon}`.
**Vasakpoolseks ümbruseks** aga arvtelje vahemikku arvust :math:`a - {\epsilon}` kuni :math:`a`.

Ehk vasakpoolne ümbrus on see, mis jääb a-st vasakule, parempoolne see, mis jääb a-st paremale.

Funktsiooni piirväärtus
-----------------------
Kui punkt :math:`a` on funktsiooni :math:`f` määramispiirkonna punkt
ja funktsioon :math:`f` on elementaarfunktsioon, siis

:math:`\lim_{x \rightarrow a} f(x) = f(a)`

Kui punkt a ei ole määramispiirkonna punkt, siis võivad tekkida määramatused.
:math:`0/0, \infty/\infty, \infty -\infty, 0\infty, 0^0, 1^\infty, \infty^0`

Määramatuse kõrvaldamiseks on hulk võtteid:

    -   teguriteks lahutamine
    -   irratsionaalsuse üleviimine lugejast nimetajasse ja vastupidi
    -   tuntud piirväärtuste ärakasutamine
    -   muutuja vahetus

Piirväärtust on vaja teatud protsesside kirjeldamiseks lühidalt.

Näide 1
^^^^^^^

Näitest näeme, et vaadeldavat funktsiooni ei eksisteeri punktis 2.
Aga mis juhtub punkti 2 ümbruses? Selle uurimiseks määrame punkte 2-e ümbrusest (üleval argumendi, all funktsiooni väärtused.).
Märkame, et funktsiooni väärtused liiguvad arvu 4 poole.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/piirvaartus_naide.PNG
   :width: 300pt
   :align: center

Funktsiooni graafik on auguga sirge (auk tähistatud mulliga).

Kui argumendi väärtuse (arvu 2) ümbruse vähendamisega saab
muuta funktsiooni vastava väärtuse (arvu 4) ümbruse kuitahes
väikeseks, siis öeldakse, et arv 4 on funktsiooni :math:`f(x)` piirväärtus,
kui x läheneb arvule 2 ja kirjutatakse:

:math:`\lim_{x \rightarrow 2} \f(x) } = \lim_{x \rightarrow 2} \frac{x^2 - 4}{x - 2} = 4`

Funktsiooni piirväärtus ei ütle, missugune on funktsiooni väärtus
**vaadeldaval kohal**, ta iseloomustab vaid funktsiooni väärtusi
**vaadeldava koha ümbruses**.

Näide 2
^^^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/piirvaartus_naide_2.PNG
   :width: 300pt
   :align: center

Mida siis piirväärtus antud juhul meile annab?

Lubab meil kompaktselt öelda, et kui argumendi väärtused lähenevad 0-le,
siis funktsiooni väärtused lähenevad 1-le.

Funktsiooni omaduste kirjeldamine piirväärtusega
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Näiteks eksponentfunktsioon.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/piirvaartus_kirjeldamine.PNG
   :width: 300pt
   :align: center

Kui argumendi väärtused tõkestamatult kasvavad, siis lähenevad funktsiooni väärtused tõkestamatult nullile.
Tähistame argumendi väärtuste tõkestamatut kasvamist sümboliga :math:`\infty`.

:math:`\lim_{x \rightarrow \infty} a^x = 0`

Kokkuvõte: saame kokku panna kogu jutu piirväärtusega. Pika jutu asemel lühike kokkuvõte.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/piirvaartus_kirjeldamine2.PNG
   :width: 300pt
   :align: center

Kui argumendi väärtused tõkestamatult kahanevad, siis lähenevad funktsiooni väärtused tõkestamatult nullile.
Tähistame argumendi väärtuste tõkestamatut kahanemist sümboliga :math:`-\infty`.

:math:`\lim_{x \rightarrow -\infty} a^x = 0`

Piirväärtuse definitsiooni eelmäng
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
**Tähistame:**

-   argumendi :math:`x` väärtuse tähega :math:`a`
-   argumendi ümbruse raadiuse tähega :math:`\delta > 0`
-   funktsiooni :math:`f(x)` vastava väärtuse tähega :math:`A`
-   funktsiooni :math:`f(x)` vastava väärtuse ümbruse raadiuse tähega :math:`\epsilon > 0`

Saame öelda, et:

-   :math:`x` kuulub :math:`a` ümbrusesse raadiusega :math:`\delta > 0: |x - a| < \delta`
-   :math:`f(x)` kuulub :math:`A` ümbrusesse raadiusega :math:`\epsilon > 0: |f(x) - A| < \epsilon`

Mida need laused tähendavad?

-   Mul on selline arv x, mis peab jääma sellesse piirkonda. Absväärtust saab alati lahti rääkida ka kaugusega.
    Arv x mahub siia ära nii, et x-i ja a vaheline kaugus on deltast väiksem.

-   Mingisugune funktsiooni väärtus peaks ära mahtuma sellise ala peale.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/delta_selgitus.PNG
   :width: 300pt
   :align: center

Piirväärtuse definitsioon matanalüüsi vaatest
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Arvu A nimetatakse funktsiooni f piirväärtuseks kohal a, kui
iga arvu :math:`\epsilon > 0` korral leidub selline arv :math:`\delta > 0`, et kehtib võrratus
:math:`|f(x) - A| < \epsilon` alati, kui :math:`|x - a| < \delta` ja kirjutatakse

:math:`\lim_{x \rightarrow} f(x) = A`

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/piirvaartus_definitsioon.PNG
   :width: 300pt
   :align: center

Ehk kui kehtib :math:`|x - a| < \delta`, siis kehtib ka võrratus :math:`|f(x) - A| < \epsilon`.
Kui x asub a ümbruses, siis on :math:`f(x)` väärtus alati A ümbruses. Alusta lause lugemisel teisest tingimusest.

Pildiga tahetakse öelda, et kui võtan x-teljel olevas pallikeses väärtuse ja hakkan teda kokku suruma,
siis A on vaadeldava funktsiooni piirväärtuseks kohal a juhul, kui a kokkusurumisega surutakse kokku ka A pall.

**NB! Definitsioon on raske!!**

Tõestamine
^^^^^^^^^^
:math:`\lim_{x \rightarrow 2} (3x + 1) = 7`

Tõestad, ei arvuta. Arvutamine on lihtne - paned 2-e x-i asemele.
Tõestamine käib eeltoodud definitsiooni kaudu. Selle kaudu tuleb näidata, et tulemus tuleb 7.

Definitsioonis on kaks võrratust ja kirjutame need abiks välja.

:math:`|f(x) - A| < \epsilon` ja :math:`|x - a| < \delta`

Järgnevalt paneme rollidesse meie ülesande andmed.
Näeme, et funktsioon on 3x + 1. Tulemuseks tulev piirväärtuse A on 7 ja X-telje väärtus, millele lähenetakse, on 2.

Ehk siis :math:`f(x) = 3x + 1`, :math:`A = 7`, :math:`a = 2`

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/piirvaartus_toestus.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/piirvaartus_toestus_2.PNG
   :width: 300pt
   :align: center

Funktsiooni ühepoolsed piirväärtused
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Kui tavaliste piirväärtuste käsitlemine kuulub gümnaasiumiprogrammi,
siis ühepoolseid üldiselt ei vaadata.

Näeme, et nullile lähenedes vasakult või paremalt paneb funktsiooni väärtus punuma kas miinus või pluss lõpmatusse.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/uhepoolsed_piirvaartused.PNG
   :width: 300pt
   :align: center

Kui tahame rõhutada, et läheneme nullile paremalt, siis selleks on omad märgendid, antud juhul pluss-märk nulli taga.
Kindlasti nulli taga - ta on suunanäit.

Mis on võrdusmärgi kohal olev murdarv? Loogika on selles, et tahame määrata ära tulemuse märki.
Kui läheneme negatiivsusest, siis nimetajas ei muutu midagi, sest seal pole muutujat x.
Küll aga muutub lugeja - kuna läheneme 0-le negatiivsusest, aga 0-i ei puuduta ja positiivsusesse üle ei hüppa,
siis on lugeja alati miinusmärgiga. Sestap paneme võrdusmärgi kohal 0-le ette miinuse. Pluss ja miinus annavad negatiivse arvu,
seega tuleb lõpmatus miinusmärgiga.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/uhepoolsed_piirvaartused_2.PNG
   :width: 300pt
   :align: center

Kui ühepoolsed piirväärtused mõlemad on lõplikud ja omavahel võrdsed, siis sellest järeldub tavaline kahepoolne piirväärtus,
millega meie oleme tuttavad varasemast koolielust. See seos kehtib ka vastupidi - kui on kahepoolne piirväärtus,
siis on olemas ka kaks ühepoolset piirväärtust, mis on omavahel võrdsed.

Definitsioon
^^^^^^^^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/uhepoolne_pv_definitsioon.PNG
   :width: 300pt
   :align: center

:math:`\lim_{x \rightarrow a} f(x) = A` siis ja ainult siis, kui
:math:`\lim_{x \rightarrow a-} f(x) = \lim_{x \rightarrow a+} f(x) = A`

Piirvaartuste arvutamise tabel
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/piirvaartuste_arvutamine.PNG
   :width: 300pt
   :align: center

Funktsiooni pidevus
-------------------
Popteaduslik variant on lihtne - kui funktsiooni graafiku sab joonistada pliiatsit paberilt tõstmata,
siis on funktsioon pidev.

Matanalüüsi jaoks on pidevuse definitsioon muu.
Funktsioon on pidev mingisugusel kohal juhul kui funktsiooni piirväärtus ja väärtus selles punktis langevad kokku.

*   :math:`\lim_{x \rightarrow a} f(x) = f(a)`

Tuleb aru saada, et definitsiooni sees on kolm eraldi nõuet:

1.  et saada rääkida võrduse paremast pooles, peab funktsioon olema määratud kohal a

    *   s.t punkt a kuulub hulka X ehk funktsiooni määramispiirkonda

    *   s.t leidub f(a) ehk funktsioonil on punktis a väärtus

2.  funktsioonil peab olema **lõplik** piirväärtus kohal a

    *   s.t leidub :math:`\lim_{x \rightarrow a} f(x)` ehk piirväärtust saab leida.

3.  peab kehtima võrdus :math:`\lim_{x \rightarrow a} f(x) = f(a)` ehk need kaks peavad olema võrdsed

Näited - kas on pidev
^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pidevus_naited.PNG
   :width: 300pt
   :align: center

Piirkonnas pidev funktsioon
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Kui varem rääkisime pidevusest punktis,
siis sageli läheb vaja pidevust

**Funktsiooni nimetatakse pidevaks piirkonnas X, kui ta on pidev piirkonna X igas punktis.**

Kui funktsioonil on mingi omadus kogu arvsirgel, siis öeldakse, et see omadus kehtib **kõikjal**.

Öeldakse, et funktsioon on **paremalt pidev** kohal :math:`a`, kui :math:`\lim_{x \rightarrow a+} f(x) = f(a)`
ja **vasakult pidev** kohal :math:`a`, kui :math:`\lim_{x \rightarrow a-} f(x) = f(a)`

**Teoreem.** Funktsioon on pidev punktis :math:`a` siis ja ainult siis,
kui ta on punktis :math:`a` pidev vasakult ja paremalt.

:math:`\lim_{x \rightarrow a-} f(x) = \lim_{x \rightarrow a+} f(x)`

**NB! Seda võrdust läheb vaja ka praktikumis.**

Pidevate funktsioonide omadused
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pvf_omadused.PNG
   :width: 300pt
   :align: center

Näites lammutame funktsiooni tükkideks. Märkame komponentfunktsioone ja näeme, et kaks esimest on koos korrutamistehtega ja kolmas miinusmärgiga.
Kokkupanekul on kasutatud korrutamist ja lahutamist ning kuna funktsioonid on selgelt pidevad, siis on ka tulemus pidev.

Kui mingi funktsioon on mingis piirkonnas määratud, siis on ta selles piirkonnas kindlasti pidev!

Liitfunktsiooni pidevus
^^^^^^^^^^^^^^^^^^^^^^^
**Teoreem.** Liitfunktsioon :math:`f[g(x)]` on pidev kohal :math:`a`, kui
:math:`g(x)` on pidev kohal :math:`a` ja :math:`f[g(x)]` on pidev kohal :math:`a`

Ehk teisisõnu - liitfunktsioon on pidev, kui selle koostisosad on pidevad.
See tulemus kehtib ka siis, kui liitfunktsioonil on mitu koostisosa.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pidevus_naited_2.PNG
   :width: 300pt
   :align: center

Teoreeme pidevatest funktsioonidest
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Hea teada silmaringi avardamiseks.
Viimast võib minna vaja võrrandite ligikaudse lahendamise programmeerimiseks.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/teoreeme_PvF.PNG
   :width: 300pt
   :align: center

Katkev funktsioon
^^^^^^^^^^^^^^^^^
Nendel funktsioonidel on üks ühine joon - neil on sama määramispiirkond, mille väärtuste väljaarvutamisel on vaid üks takistav klausel ehk 0 jääb välja.

Esimese puhul märkame, et 0-i koha peal toimub hüppeline muutus.
Teisel pildil võib näida, et funktsioon on pidev, kuid nulli kohal funktsioon katkeb, sest 0 ei kuulu määramispiirkonda.
Kolmandal pildil nulli juures läheb üks graafiku haru :math:`\infty` suunas ja teine :math:`-\infty` suunas.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/katkevus.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/katkevus2.PNG
   :width: 300pt
   :align: center

Kõigil kolmel katkiolemise viisil on omad nimed:

1.  Esimest liiki katkevuse alaliik - hüppekoht
2.  Esimest liiki katkevuse alaliik - kõrvaldatav katkevus
3.  Teist liiki katkevus - kui pistab lõpmatuse poole punuma.

**Definitsioon**
Funktsiooni katkevuspunktiks nimetatakse punkti, milles funktsioon ei ole pidev.

Esimesse liiki kuulub katkevuspunkt, kus funktsioonil on olemas ühepoolsed piirväärtused,

    * :math:`f(a+) = \lim_{x \rightarrow a+} f(x)`

    * :math:`f(a-) = \lim_{x \rightarrow a-} f(x)`

Kõik ülejäänud katkevuspunktid on 2. liiki.

Esimese alamjuhtude puhul on näha, et piirväärtused on kenasti lõplikud 1 ja -1 ning 1 ja 1.

Esimest liiki katkevuspunktide jaotus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
1. Hüppekoht

    Arvu :math:`a` nimetatakse funktsiooni :math:`y = f(x)` hüppekohaks, kui

    *   :math:`\lim_{x \rightarrow a-} f(x) \noteql \lim_{x \rightarrow a+} f(x)`

Kui ühepoolsed piirväärtused on olemas ja lõplikud, kuid mittevõrdsed.
Nad ei pea olema vastandarvud, vaid piisab erinevusest.

2. Kõrvaldatav katkevuskoht

    Arvu :math:`a` nimetatakse funktsiooni :math:`y = f(x)` kõrvaldatavaks katkevuskohaks, kui

    *   :math:`\lim_{x \rightarrow a-} f(x) = \lim_{x \rightarrow a+} f(x) = \lim_{x \rightarrow a} f(x) \text{ ja } a \notin X`

Siis, kui ühepoolsed piirväärtused on lõplikud ja omavahel võrdsed, kuid vaadeldav punkt ei kuulu funktsiooni määramispiirkonda.

Katkevuse kõrvaldamiseks määratakse täiendavalt funktsioooni väärtus kohal :math:`a` tingimusega

    *   :math:`f(a) = \lim_{x \rightarrow a} f(x)`

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/katkevuspunkt.PNG
   :width: 300pt
   :align: center

Ahelavaldise esimene rida ütleb, et funktsiooni määramispiirkonnas igal pool, kus pole tegemist katkemispunktiga, jääb esialgne funktsiooni avaldus kehtima.

Teine rida ütleb, et seal kus funktsiooni väärtus pole määratud, anname funktsioonile vägisi juurde kahepoolse piirväärtuse.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/katkevuspunkt_naide.PNG
   :width: 300pt
   :align: center

Täidame puuduoleva punkti ja saame pideva funktsiooni.

NB! Alati pole vaja katkevaid funktsioone sedasi täiendada, kuid kui on vaja, siis seda saab teha.

Esimest liiki katkevuspunktid
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Arvu :math:`a` nimetatakse funktsiooni :math:`y = f(x)` teist liiki katkevuspunktiks kui

    *   :math:`\lim_{x \rightarrow a-} f(x)` on lõpmatu või ei eksisteeri

    *   :math:`\lim_{x \rightarrow a+} f(x)` on lõpmatu või ei eksisteeri

    *   (s.t kui :math:`\lim_{x \rightarrow a} f(x)` on lõpmatu või ei eksisteeri)

Näide:

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/katkevus_naide2_2.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/katkevus_naide2.PNG
   :width: 300pt
   :align: center

NB! Lõpmatuse märgid tuleb ära määrata!! Ilma ei saa.

Teist liiki katkevuspunktid
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Arvu :math:`a` nimetatakse funktsiooni :math:`y = f(x)` teist liiki katkevuspunktiks, kui

    -   :math:`\lim_{x \rightarrow a-} f(x)` on lõpmatu või ei eksisteeri
    -   :math:`\lim_{x \rightarrow a+} f(x)` on lõpmatu või ei eksisteeri
    -   s.t kui :math:`\lim_{x \rightarrow a} f(x)` on lõpmatu või ei eksisteeri

Piirväärtuse ülesanded
^^^^^^^^^^^^^^^^^^^^^^
Ülesanne 1

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pv_ex01.PNG
   :width: 300pt
   :align: center

Mis siin ülesandes sisuliselt teeme?
Meil oli vaja arvutada piirväärtus, aga arvutasime funktsiooni väärtuse punktis 1.

    -   Selgub, et antud juhul langevad funktsiooni väärtus ja piirväärtus lihtsalt kokku.
    -   Miks? Sest kui vaadelda punkti a ja see on määramispiirkonna punkt,
        siis langevad väärtus ja piirväärtus alati kokku.
    -   Me pole küll leidnud MP-d, kuid aimame, et küll ta sinna kuulub.
    -   Tegelikult tuleb MP-s :math:`[0.1;10]` ja 1 kuulub sinna. Seepärast kattuvadki a ja pv.

Sellised on kõige lihtsamad ülesanded. Huvitavamad on sellised, kus nii lihtsalt vastus ei tule.
Kui punkt a ei ole funktsiooni MP punkt, siis võivad tekkida määramatused.

Meil võivad tekkida :math:`0/0, \infty/\infty, \infty - \infty`.

Funktsiooni tuletis
===================
Argumendi ja funktsiooni muut
-----------------------------
:math:`\Delta x` on argumendi väärtuste muut.
:math:`\Delta x = x_2 - x_1`

:math:`\Delta y` on funktsiooni väärtuste muut.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/muut.PNG
   :width: 300pt
   :align: center

Funktsiooni muudu üldavaldis
----------------------------
:math:`\Delta y = f(x + \Delta x) - f(x)`

Vajalik selleks, et saada teada kui palju muutuvad funktsiooni ehk :math:`y` väärtused,
kui argumendi väärtused muutuvad  :math:`\Delta x` võrra. Põhimõte on lõpp-punkt miinus alguspunkt.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/muudu_yldavaldis.PNG
   :width: 300pt
   :align: center

Näide
^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/muut_yldavaldis_naide.PNG
   :width: 300pt
   :align: center

Graafik
^^^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/muut_naide_graafik.PNG
   :width: 300pt
   :align: center

Tuletis
-------
Tuletis pole midagi muud kui teatud tüüpi piirväärtus - võetakse funktsiooni muudu ja argumendi muudu jagatisest
protsessis, kus argumendi muut läheneb nullile.

:math:`\lim_{\Delta t \rightarrow 0} \frac{\Delta s}{\Delta t}`

Funktsiooni :math:`y = f(x)` tuletiseks :math:`y = f'(x)` kohal :math:`x` nimetatakse piirväärtust

:math:`f'(x) = \lim_{\Delta x \rightarrow 0} \frac{\Delta y}{\Delta x} = \lim_{\Delta x \rightarrow 0} \frac{f(x + \Delta x) - f(x)}{\Delta x}`

kui see piirväärtus eksisteerib ehk on lõplik.

Tuletise tähised
----------------
:math:`f'(x)`, :math:`y'`, :math:`\frac{dy}{dx}`, :math:`\frac{df(x)}{dx}`.

Viimased kaks on eeliseks see, et on täpselt ära näidatud milline on funktsioon ja milline on muutuja, mille järgi tuletist võetakse.
Hea, kui muutujaid on funktsioonis mitu.

Tuletise leidmise skeem
-----------------------
Tuletise leidmine kasutades tuletise definitsiooni. **Leidmine definitsiooni järgi**, mis on omaette ülesandetüüp!!

See on see valem: :math:`f'(x) = \lim_{\Delta x \rightarrow 0} \frac{f(x + \Delta x) - f(x)}{\Delta x}`

Koosneb järgmistest etappidest:

    1.  Kõigepealt kirjutatakse välja funktsiooni muut.

        -   funktsiooni :math:`f'(x)` muudu :math:`\Delta y` arvutamine vastavalt valemile
            :math:`\Delta y = f(x + \Delta x) - f(x)`.

    2.  Moodustatakse funktsiooni muudu ja argumendi muudu jagatis.

        -   :math:`\frac{\Delta y}{\Delta x}`

    3.  Lõpuks võetakse kogu tulemusest piirväärtus.

        -   :math:`\lim_{\Delta x \rightarrow 0} \frac{\Delta y}{\Delta x}`

**Etappideks jagamise mõte**: igal sammul saab lihtsustada.

Näide
^^^^^
.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/tuletis_naide.PNG
   :width: 300pt
   :align: center

See on tuletise mehhaanika-alane lahtikirjeldus, põhjendus.
Kuid on ka geomeetriline põhjendus ja selleks on meil vaja defineerida joone puutuja.

Joone puutuja
-------------
Kõverjoon, millel on märgitud punktid P ja Q. Joonistame läbi nende lõikaja.
Kui hakkame vaatama, mis lõikajaga juhtub, kui punkt Q hakkab mööda joont P poole liikuma.
Pildil vaheseisud. Lõpuks jõuame definitsioonini, et joone puutujaks punktis P nimetatakse
lõikaja PQ piirseisu, kui punkt Q piiramatult läheneb punktile P mööda kõverat.

:math:`\beta` on siin lõikaja ja x-telje vaheline nurk
ja :math:`\alpha` on puutuja ja x-telje vaheline nurk.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/joone_puutuja.PNG
   :width: 300pt
   :align: center

Edasi uurime, mis juhtub, kui Q hakkab P poole sõudma.

    -   :math:`\Delta x \rightarrow 0` ehk delta x läheneb nulli ehk x-i välja.

    -   :math:`\beta \rightarrow \alpha` ehk lõikajast saab puutuja.

    -   ja kui beetast saab alfa, siis saa ka :math:`tan\beta \rightarrow tan\alpha`

Kõrgemas matis kasutatakse peamiselt definitsiooni järgi lahendamist,
kuid geomeetrilist lahendust peetakse elementaarseks, mistõttu peame teadma ka seda.

Funktsiooni diferentseeruvus
----------------------------
Funktsiooni diferentseeruvuseks nimetatakse **funktsiooni tuletise leidmist**.

Kui funktsioonil :math:`y = f(x)` on tuletis punktis :math:`x = x_0`,
siis ütleme, et *funktsioon on diferentseeruv punktis :math:`x_0`*.

Kui funktsioon on diferentseeruv aga mingi piirkonna igas punktis,
siis öeldakse, et see *funktsioon on diferentseeruv selles piirkonnas*.

**Teoreem.** Kui funktsioonil on antud kohal lõplik tuletis,
siis funktsioon on sellel kohal **pidev**.

NB! See lause kehtib vaid seda pidi. Ta pole pööratav.
Ehk pidevusest ei pruugi järelduda tuletise olemasolu.

**Keerulisemalt öeldes**: pidevus on diferentseeruvuse tarvilik, kuid mittepiisav tingimus,
sest leidub funktsioone, mis on pidevad, kuid neil pole tuletist mõnedel :math:`x` väärtustel.

Näide
^^^^^
.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/difpidevus_naide.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/dif_naide.PNG
   :width: 300pt
   :align: center

Siit tuleb välja, et funktsioon on küll pidev, kuid tal pole 0-s piirväärtust.

Diferentseerimise põhivalemid
-----------------------------
Reeglid põhifunktsioonidele
^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/dif_pohivalemid.PNG
   :width: 300pt
   :align: center

Tehetega seotud diferentseerimisreeglid
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/dif_tehted.PNG
   :width: 300pt
   :align: center

-   Liitmise reegel: summa puhul võid võtta tuletise igast liikmest eraldi.
-   Lahutamise reegel: vahe puhul võid võtta tuletist igast liikmest eraldi.
-   Korrutamise reegel: pead korrutama esimese liikme tuletist teise liikmega ning liitma ta teise liikme tuletise ja esimese liikme korrutisega.
-   Konstandi reegel: järeldus korrutise valemist, mis ütleb, et kui üks tegur on arv, siis korrutad arvu teise teguri tuletisega.
-   Jagamise reegel: siin aitab see, et lugejas on korrutamise reegel aga miinusega!. Ära unusta, et nimetajasse jääb esialgse **nimetaja ruut**!

Näide
^^^^^
.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/dif_naide2.PNG
   :width: 300pt
   :align: center

Neid trigteisendusi PEAB teadma
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/trig_peab_teadma.PNG
   :width: 300pt
   :align: center

Liitfunktsiooni diferentseerimine
---------------------------------
Liitfunktsiooni tuletis on välimise funktsiooni tuletis korda sisemise funktsiooni tuletis.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/liitf_dif.PNG
   :width: 300pt
   :align: center

Valemina pole neid õppida mõtet. Eelkõige on vaja tuletisi osata leida.
Vaja on teada, et liitfunktsiooni tuletis on välimise funktsiooni tuletise ja sisemise funktsiooni tuletise korrutis.

Liitfunktsioonis tuletiste leidmiseks on vaja esmalt leida kõik funktsioonid
ehk liitfunktsioon tükeldada.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/liitf_naide.PNG
   :width: 300pt
   :align: center

Näide absoluutväärtustega
^^^^^^^^^^^^^^^^^^^^^^^^^
Loo moraal: absoluutväärtuse märgid rolli mängima ei jää.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/dif_absol.PNG
   :width: 300pt
   :align: center

Kontrollnäide
^^^^^^^^^^^^^
NB! Kirjutasid selle ka vihikusse!

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/dif_kontroll.PNG
   :width: 300pt
   :align: center

Logaritmiline diferentseerimine
-------------------------------
Liitfunktsioonide lahendus tavavõtetega võib vahel olla pikk ja veaohtlik.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/teisend_naide.PNG
   :width: 300pt
   :align: center

Kõigepealt astendame nii, et juurikat astendajasse ei jää.
Võtan tegurhaaval. Pea meeles et kuupjuure astendaja on 1/3.
Nimetajast toome teguri välja miinusega.

Edasi logaritmin avaldise. Kuna sellist tüüpi funktsioonid võivad ka olla negatiivsed,
siis paneme absoluutväärtuse märgid ka ümber. Tapa võimalik negatiivsus!
Ära saab jätta vaid siis, kui negatiivsus pole võimalik.

Nüüd asun lihtsustama logaritmide omadusi kasutades.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/log_omadused.PNG
   :width: 150pt
   :align: center

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/teisend_naide_jatk.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/dif_log.PNG
   :width: 300pt
   :align: center

NB! Kasu on sellest, et oskad reaalse funktsiooni puhul kõike seda järgi teha.

Astmefunktsiooni tuletis
------------------------
Mis tähendab aste omab mõtet?

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/dif_astmef.PNG
   :width: 300pt
   :align: center

Astme-eksponentfunktsioonid
---------------------------
Astmefunktsiooni puhul eeldatakse, et astendaja on arv.
Eksponentfunktsiooni puhul eeldatakse, et alus on arv.

Meil on juht, kus nii astme alus kui ka astendaja sisaldavad tundmatut.
Ta on nagu segu astme- ja eksponentfunktsioonist ehk samas mitte kumbki.

Astmefunktsiooni kuju:

    -   :math:`u(x)^{v(x)}`, :math:`u(x) > 0`

Peaks teadma, et astmefunktsioonis on astendajas ja aluses tundmatu.
NB! **Need funktsioonid on defineeritud vaid siis, kui astme alus on positiivne.**
Tagab ka selle, et astmefunktsiooni väärtus on alati positiivne.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/astmef_tuletis.PNG
   :width: 300pt
   :align: center

Kuidas käib nende tuletiste leidmine?

Ainus viis on logaritmiline diferentseerimine.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/astmef_tuletis.PNG
   :width: 300pt
   :align: center

Funktsiooni diferentsiaal
-------------------------
Funktsiooni :math:`y = f(x)` **diferentsiaaliks** :math:`dy` nimetatakse avaldist :math:`dy = f'(x) \Delta x`.

Funktsiooni diferentsiaal näitab, kui palju muutuvad funktsiooni väärtused, kui argument :math:`x` muutub suuruse :math:`\Delta x` võrra
ja liikumine mööda graafikut on asendatud liikumisega mööda puutujat.

d järel olev täht ütleb, mis funktsiooni diferentsiaalist meil jutt käib.

Diferentsiaali geomeetriline tõlgendus
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Meil on funktsiooni graafikul punkt P, mille üks koordinaatidest on x.
Seejärel joonistan punkti P läbiva puutuja ning märgistan puutuja ja x-telje vahelise nurga tähisega :math:`\alpha`.
Nüüd valin joonel veel ühe punkti Q, mille sain siis, kui liikusin x-teljel edasi :math:`x + \Delta x` võrra.

Edasi võin täiendada joonist täisnurkse kolmnurgaga, mis on hetkel värvitud - kolmnurk PRS.
Sealt võin edasi avaldada teravnurga tangensi. Edasi võtan võrdusest ristkorrutise.
Edasi võin märgata, et PR pole midagi muud kui minu :math:`\Delta x`.
Eelmisest loengust pean teadma, et :math:`tan \alpha`, ehk puutuja tõus, on ühtlasi funktsiooni tuletis kohal x ehk :math:`f'(x)`.
See oli tuletise geomeetriline definitsioon.

Nende kahe teadmise kokkupanemisel saan valemi, mis ütleb et :math:`RS = PR \times tan \alpha = f'(x) \Delta x`.

Seda komplekti tähistan :math:`dy`. Seda leiutist - :math:`dy` on funktsiooni tuletis korda argumendi muut ehk :math:`f'(x) \times \Delta x`.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/diferentsiaal.PNG
   :width: 300pt
   :align: center

Kokkuvõte: geomeetriliselt kujutab funktsiooni diferentsiaal graafiku puutuja ordinaadi muutu.

Lause sisu: mida näitab funktsiooni muut ja mida näitab diferentsiaal?

Funktsiooni muut näitab kui palju muutuvad funktsiooni väärtused, kui argument :math:`x` muutub suuruse :math:`\Delta x` võrra
ja liikumine toimub mööda funktsiooni graafikut.

Funktsiooni diferentsiaal näitab, kui palju muutuvad funktsiooni väärtused, kui argument :math:`x` muutub suuruse :math:`\Delta x` võrra
ja liikumine mööda graafikut on asendatud liikumisega mööda puutujat.

Jäta meelde: :math:`dy` tähistab puutujat, :math:`\Delta y` tähistab funktsiooni graafikut.

** NB! Tuletise kõrval on funktsiooni muut ja diferentsiaal sellised asjad, mida peab teadma!!**

Kuidas on funktsiooni muut ja diferentsiaal omavahel seotud?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/diferentsiaal.PNG
   :width: 300pt
   :align: center

Keskendu pildil sellele, et argumendi muut on hetkel üsna pikk lõik. Kui võrdled omavahel
:math:`dy`-t ja :math:`\Delta y`-t ehk funktsiooni diferentsiaali ja muutu,
siis on näha, et nende pikkused on üsna erinevad.
Uue joonise puhul on :math:`\Delta x` läinud õige pisut väiksemaks ning sellega koos on läinud väiksemaks :math:`dy`-t ja :math:`\Delta y` erinevus.

**Siit koorub välja oluline teadmine:**

    -   "Väikese" :math:`\Delta x` korral :math:`\Delta y \approx dy`.
    -   "Väikese" argumendi väärtuste muudu korral on funktsiooni väärtuste muut ja diferentsiaal ligikaudu võrdsed.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/diferentsiaali_arvutamine.PNG
   :width: 300pt
   :align: center

Miks see tähtis on?

Annab õiguse, juhul kui argumendi muut on väike, kasutada funktsiooni muudu arvutamise asemel diferentsiaali leidmist.
**Muudu leidmine on tavaliselt töömahukas, samas kui diferentsiaali leidmine on tükk maad lühem ja tuletise leidmise oskuse olemasolul kergem. Lubab teatud ülesannetes kasutada ära diferentsiaalil**

Argumendi diferentsiaal
^^^^^^^^^^^^^^^^^^^^^^^
Kui :math:`f(x) = x`, siis **argumendi diferentsiaal** :math:`dx` avaldub kujul :math:`dx = x' \Delta x = 1 \times \Delta x = \Delta x`.
Ehk argumendi diferentsiaal ja argumendi muut on võrdsed.

Alati kehtib, et :math:`\Delta x = dx`. See annab võimaluse kasutada valemis emba-kumba.

Seega seose :math:`dy = f'(x) \Delta x` põhjal võin kirjutada :math:`dy = f'(x) dx` ehk :math:`f'(x) = \frac{dy}{dx}`.

Ehk funktsiooni tuletis on funktsiooni diferentsiaali ja argumendi diferentsiaali jagatis.

Näited 1
^^^^^^^^
.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/diferentsiaal_näited.PNG
   :width: 300pt
   :align: center

Diferentsiaali kasutamine ligikaudses arvutamises
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Väikese :math:`\Delta x` korral :math:`\Delta y = dy`.
Väikese *argumendi muudu* korral on *funktsiooni muut* võrdne *funktsiooni diferentsiaaliga*.

Asendan siin funktsioooni muudu ja funktsiooni diferentsiaali oma vastavate valemitega:

:math:`f(x + \Delta x) - f(x) \approx f'(x) \times \Delta x`

Siis viin :math:`f(x)`-i teisele poole ehk:

:math:`f(x + \Delta x) \approx f(x) + f'(x) \times \Delta x`

See valem on laialdases kasutuses. Vaatame näidet.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/diferentsiaal_ligikaudne.PNG
   :width: 300pt
   :align: center

Absoluutne ja relatiivne viga
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Lähendi veaks ehk tõeliseks veaks (:math:`\Delta x`) nimetame täpse arvu :math:`X` ja tema ligikaudse väärtuse ehk lähendi :math:`x` korral suurust :math:`|X - x|`.

    -   :math:`\Delta x = X - x` ehk mõõtmisel tehtud tegelik viga
    -   :math:`|X - x|` kui vea suund ei huvita

**Absoluutne viga** (:math:`\delta x`)

Tavaliselt pole võimalik tõelist viga leida, vaid saab hinnata millist arvu viga ei ületa.

Seda hinnangut :math:`\delta x` **nimetataksegi absoluutseks veaks**.

Võime kirjutada välja, et mõõtmise absoluutne viga on hinnang täpse väärtuse ja ligikaudse väärtuse erinevusele (:math:`|X - x| \leq \delta x`).

**Relatiivne viga**
Mõiste: relatiivne ehk suhteline viga on ligikaudse arvu absoluutse vea ja selle arvu absoluutväärtuse jagatis.

    -   :math:`\frac{\delta x}{|x|}`

Olgu meil kaks mõõtmist: ühe laua läbimõõt on 50cm ja teise laua läbimõõt on 500cm.
Kui kasutasime mõõtmiseks sama vahendit, siis mõõtmise absoluutne viga on mõlemal puhul sama (:math:`\delta x = 1 cm`).

Vea panus 500cm läbimõõduga on oluliselt väiksem kui 50cm omas. See tähendab, et absoluutne viga ei võimalda mõõtmistulemusi võrrelda.
Võrdluse saamiseks kasutatakse relatiivset viga ehk suhtelist viga.

Relatiivne viga on absoluutse vea ja mõõtmistulemuse jagatis. :math:`\frac{\delta x}{x}`

Esimesel juhul on see :math:`\frac{\delta x}{x} = \frac{1}{50} = 2%` ja teisel juhul :math:`\frac{\delta x}{x} = \frac{1}{500} = 0,2%`.

**Diferentsiaali kasutamine mõõtmisvigade hindamisel**
    -   :math:`x` ehk mõõtmisel saadud tulemus.
    -   :math:`\Delta x` ehk suuruse :math:`x` mõõtmisel tehtud tegelik viga.
    -   :math:`\delta x` ehk suuruse :math:`x` mõõtmise maksimaalne absoluutne viga :math:`x(\pm \delta x)`).

Tegelik viga on hinnatav mõõtmise absoluutse veaga. Kehtib võrdus :math:`\Delta x \leq \delta x`

Suurus :math:`x` mõõdetakse otseselt, suurus :math:`y` aga kaudselt valemi :math:`y = f(x)` järgi.

Funktsiooni muudu arvutamise võib asendada diferentsiaali arvutamisega, kuna :math:`\Delta y \approx f'(x) \Delta x`.
Kui võtan ligikaudse valemi ja hindan tema kõiki liikmeid, ehk kirjutan kõikide liikmete ümber absoluutväärtuste märgid,
siis saan seose :math:`|\Delta y| \approx |f'(x)| \times \Delta x \leq |f'(x)| \times \delta x`.

See tähendab, et hindan tegeliku vea mõõtmise absoluutse maksimaalse veaga ehk kasutan võrratust.

See annab kokku valemi abil arvutatud suuruse absoluutse vea valemi, mis on suuruse :math:`y` **maksimaalne absoluutne viga**
ehk :math:`\delta y = |f'(x)| \delta x`.

Valem ütleb: valemi abil arvutatud **suuruse absoluutne viga** (:math:`\delta y`) on võrdne arvutusvalemi tuletise ja mõõtmise absoluutse vea korrutisega.

Leidsime eeskirja, kuidas arvutada laua pindala arvutamisel tehtav viga.
Et teada saada kuidas hinnatatakse valemi abil suuruse absoluutset viga.

Vastav **maksimaalne relatiivne viga** arvutatakse: absoluutne viga jagatakse valemi abil arvutatud suurusega.

Valem on:

    -   :math:`\frac{\delta y}{|y|} = | \frac{f'(x)}{f(x)} | \times \delta x`

    -   :math:`vastav relatiivne viga = \frac{abs. viga}{valemi abil arv. suurus}`

Peaksime oskama lahendada sellist tüüpi ülesannet, mida näeme sellel slaidil.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/mootmisviga.PNG
   :width: 300pt
   :align: center

Integraal
---------
Integreerimine on tuletise pöördtehe.
Kui tuletise kohta võime öelda, et ta lubab meil nähtust kirjeldava funktsiooni korral kirjeldada nähtuse kulgemise intensiivsust,
näiteks liikumisseaduse põhjal liikumiskiirust, siis integraal võimaldab intensiivsuse põhjal leida nähtust kirjeldavat funktsiooni.

Abimõisted
----------
Algfunktsioon
^^^^^^^^^^^^^
Funktsiooni :math:`F(x)` nimetatakse funktsiooni :math:`f(x) \text{ **algfunktsiooniks** piirkonnas } A \text{, kui } F'(x) = f(x) \text{ iga } x \in A \text{ korral }`.
Ehk teisisõnu juhul, kui suure F-i tuletis on võrdne väikese f-iga.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/algfunk_naide.PNG
   :width: 300pt
   :align: center

Küsimus - kas :math:`\frac{x^3}{3}` on funktsiooni :math:`f(x) = x^2` ainuke algfunktsioon?
Ei ole. Näiteks saad lisada konstante :math:`(\frac{x^3}{3} + 1)` ja ikka tuleb :math:`x^2`.

*Teoreemid*

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/algfunktsioon_teoreemid.PNG
   :width: 300pt
   :align: center

Määramata integraal
^^^^^^^^^^^^^^^^^^^
**Funktsiooni integreerimine** on funktsiooni algfunktsiooni leidmine.

:math:`\int f(x)dx`

Integraali märk on :math:`\int`, sulgudes :math:`x` on integreerimismuutuja,
:math:`f(x)` on integreeritav funktsioon ning :math:`f(x)dx` on integreeritav avaldis.

**NB! Integreeritav funktsioon ja integreeritav avaldis pole üks ja seesama asi.**
Tähtis ülesannete mõistmiseks.

**Määramata integraaliks** nimetatakse avaldist :math:`\int F(x) + c \text{, kus } F(x) \text{ on funktsiooni } f(x) \text{ mingi algfunktsioon }`
ja :math:`c \in \mathbb{R}` on suvaline konstant.

Kõike seda tähistatakse kujul

    -   :math:`f(x) dx = F(x) + c`

Konstant :math:`c` on **integreerimiskonstant**.

Kui keegi küsib määramata integraali definitsiooni ja paned ainult valemi, siis pead ära selgitama mida tähendavad :math:`F(x) \text{ ja } c`.

Geomeetria seisukohalt on määramata integraal joonte kogum ehk joonte parv,
kus iga joon saadakse ühe ja sama joone nihutamisel paralleelselt iseendaga kas üles või alla.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/joonte_parv.PNG
   :width: 300pt
   :align: center

Põhiintegraalide tabel
^^^^^^^^^^^^^^^^^^^^^^
Kuidas meelde jätta? Ära mõtle otseselt valemile, vaid mõtle nii, et integreerimine ja diferentseerimine on tuletise võtmise pöördtehe.
NB! Igale poole tuleb lisada konstant.

1.  :math:`\int 0 dx = c` - mis on see funktsioon, mille tuletis on 0?

Kui tuletised on hästi selged, siis peaks tulema ka vastus, et see funktsioon, millest tuletise võtmisel nulli saan, on mingi konstant ehk arv.
On vaid kaks valemit, mis tuleks pähe õppida: astmefunktsiooni (2) ja eksponentfunktsiooni integreerimise (5) valem.

2.  :math:`\int x^a dx = \frac{x^a+1}{a+1} + c \text{ , kui } a ≠ -1`

AF integreerimisel astendaja kasvab ühe võrra ja jagatakse uue astendajaga.

3.  :math:`\int dx = x + c`

Võiksid mõelda, et :math:`\int dx = \int 1 \times dx`. Mõtle, mis funktsiooni tuletis annab 1-e? See on x'.
Tuletiseks annab ühe mitte ainult x', vaid ka x + mingi arv ehk c.

4.  :math:`\int \frac{dx}{x} = \ln |x| + c`

Peaksid märkama, et :math:`\int \frac{dx}{x} = \int \frac{1}{x} \times dx`.
Sedasi lahku kirjutades peab teadma, et funktsiooni ja dx-i vahele käib alati kordusmärk.

5.  :math:`\int a^x dx = \frac{a^x}{\ln a} + c`

6.  :math:`\int e^x dx = e^x + c`

Siin kehtib patsiendi jutt. Miks sina mind ei karda? Mina olen :math:`e^x`, kellele tuletise võtmine ja integreerimine ei mõju.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/integraalide_tabel.PNG
   :width: 300pt
   :align: center

Määramata integraali omadused
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Esimene ütleb, et summa või vahe korral võib võtta integraalid funktsioonidest eraldi.

Teine ütleb, et kui integraali all on arv, siis arvu võib tuua integraali alt välja.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/int_omadused.PNG
   :width: 300pt
   :align: center

Näide
^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/omadused_naide.PNG
   :width: 300pt
   :align: center

NB! Pea meeles, et konstandi sees võib olla palju arve!

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/int_omadused2.PNG
   :width: 300pt
   :align: center

Siin tuleb meelde jätta:

    3.  Kui funktsiooni integreerimisele järgneb diferentseerimine, siis funktsioon ei muutu.

    4.  Kui diferentseerimisele järgneb integreerimine, siis funktsioon muutub konstandi võrra.

**NB! Jagamise valemit pole!** See tähendab, et funktsioon tuleb ümber mängida nii, et hammas hakkaks peale.

Vahetu integreerimine
^^^^^^^^^^^^^^^^^^^^^
Vahetul integreerimisel tohid kasutada vaid põhiintegraale ja tehetega seotud reegleid.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/vahetu_integreerimine.PNG
   :width: 300pt
   :align: center

Ositi integreerimine
^^^^^^^^^^^^^^^^^^^^
Tuntud ka kui osade kaupa integreerimine. See on kunstlik integreerimisvõte,
mis mõneti kompenseerib korrutise valemi puudumist. Päris paljud korrutised käivad ositi integreerimise valemiga.

Ta taandab esialgse raskema integraali lihtsamaks integraaliks ja see ongi tema eesmärk.

Kui :math:`u = u(x) \text{ ja } v = v()` on diferentseeruvad funktsioonid
ning leidub :math:`\int cdu`, siis leidub ka :math:`\int udv`, kusjuures

:math:`\int udv = uv - \int vdu`

Kõigepealt tuleb integreeritav avaldis jaotada kaheks:

    -   Ühte osa tähistame :math:`u`-ga ja teist :math:`dv`-ga.
        Need on juurdunud tähised.

        -   NB! Jaotada võib ainult tegureid. Näiteks ei tohi liitfunktsiooni kõhust võtta välja teist funktsiooni.

        -   (:math:`dx` käib alati :math:`dv` juurde)

    -   :math:`u` põhjal leiame :math:`du` ehk :math:`u` diferentsiaali.

    -   :math:`dv` põhjal leiame :math:`v` ehk kasutame diferentseerimise pöördtehet ehk integreerimist.

Valemit sellisel kujul pähe jätta pole mõtet.
Parem on jätta meelde nii: :math:`u \times dv` on esialgne ülesanne.
Valem ütleb, et esialgne ülesanne jaguneb kaheks tükiks.
Esimese tüki saame siis, kui võtame oma väljakirjutuse diagonaali.
Edasi tuleb miinusmärk ja korrutis mööda alumist rida.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/ositi_int.PNG
   :width: 300pt
   :align: center

Tükeldamisi on mitut võimalikku, kuid hästi lahendub vaid üks. Seega kui läheb umbe,
jaga teistmoodi.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/ositi_int_halb.PNG
   :width: 300pt
   :align: center

Teine halb variant tekib siis, kui ei saa leida v-d.

**Tarkusetera!** dv rolli tuleb alati võtta midagi sellist, mida oskad integreerida.

Muutuja vahetus ehk diferentsiaali märgi alla viimine
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Tähistame mingi osa integreeritavast funktsioonist uue tähega.
Üks võimalus on lasta silma eest läbi valemid ja mõelda,
et kui selle koha peal oleks üks täht, siis oskaksin kohe öelda palju on tulemus.

Muutuja vahetus töötab siis, kui nende kahe võrduse põhjal saab asendada kõik liikmed esialgses integraalis.

Esmalt vahetad muutuja, siis avaldad tema diferentsiaali ja siis esialgse muutuja diferentsiaali.

NB! Kui asendamist teha uue muutuja kaudu, siis koht peab olema selline, et on esindatud ainult uus muutuja. Mingit muud,
näiteks tx komplekti, ei tohi teha. See viitaks, et ülesannet nii lahendada ei saa.
Isegi kui x-id taanduvad välja, siis seda ei peeta korrektseks.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/muutujate_vahetus.PNG
   :width: 300pt
   :align: center

Nipid.

    1.  Taandada algülesanne tabeli integraalile.
    2.  Märkame integraali märgi all funktsiooni ja tema tuletist.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/nipp2.PNG
   :width: 300pt
   :align: center


Määratud integraal
^^^^^^^^^^^^^^^^^^
Määratud integraali tunnus on numbrid integraali märgi otstes.

Määratud integraali mõistmiseks meenutame trapetsi - nelinurk, kus kaks külge on paralleelsed (alused) ja kaks mitteparalleelsed (haarad).
Trapetsi levinud alamtüüp on täisnurkne trapets. Ta püsib sellisena isegi pööramisel.

On olemas ka kõvertrapets. Asendame ühe haara funktsiooniga. Kõvertrapetsid etendavad tähtsat osa tasandiliste kujundite pindalade arvutamisel.
Võib mõelda selliselt, et kui on keeruline kujund, siis võib keerulise kujundi jagada lõplikuks arvuks kõvertrapetsiteks.
Kui oskad kõvertrapetsi pindala arvuti, siis saad kätte ka üsna keeruliste kujundite pindala.

Järgnevad seletused aitavad jõuda määratud integraali definitsioonini.

Vaatame funktsiooni :math:`y = f(x)` lõigus :math:`a \text{ kuni } b`. Väljaspool asuv ala meid ei huvita.

:math:`y = f(x) \text{ lõigus } [a;b] \text{ antud funktsioon, kus } a < b, f(x) > 0`.

**NB! Määratud integraalis määramata konstanti ei tule!**

Olgu funktsioon selline, et lõigu ab piires on funktsiooni väärtused positiivsed. See tagab graafiku asumise ülevalpool X-telge.
Siis märkame koordinaattelgede abil kõvertrapets. Meid huvitab, kuidas kuju pindala kätte saab?

Kõvertrapetsi pindala leidmiseks jagame X-teljel oleva lõigu juppideks. Punktide valimist kirjeldab võrratus :math:`a = x_0 < x_1 < x_2 ... < x_n = b`.
Võrratus ütleb, et loeme punkti a võrdseks :math:`x_0`-ga ja punkti b võrdseks :math:`x_n`-iga.

Saame leida jaotuspunktidele vastavad väärtused funktsiooni graafikult ning neid väärtusi võime tähistada :math:`A = P_0 ... P_n = B`.

Sellest kõigest on niipalju kasu, et meie suure kõvertrapetsi pindala on võrdne nende väikeste kõvertrapetsite pindalade summaga.
Sellist summat saab lühemalt kirjutada summa abil: :math:`\sum`. Võrreldav tsükliga. Indeksi i väärtuseks võetakse 1
ja seda suurendatakse kuni jõuame n-ini välja.

Siit peaks silma paistma ka kavalus, et i-ndale lõigule ehitatud kujundi abil saan summa lühemalt üles kirjutada.

Edasi öeldakse slaidil, et võtame esimesel osalõigukesel ühe täiendava punkti :math:`\xi_1`,
teisel osalõigukesel punkti :math:`\xi_2` jne. Võrratusest on näha, et erijuhul võib ta langeda
kokku otspunktidega, aga ei pruugi.

Seejärel leiame funktsiooni väärtuse punktis :math:`\xi_1`. Milleks? Et ehitada esimesele lõigule ristkülik.
Samamoodi teen ka i-ndal punktil.

Osalõigukese pikkust võin tähistada :math:`\Delta x`-iga ja arvutan ta põhimõttel lõigu otspunkt lahutatud alguspunkt.
Seda tähistust kasutan, et panna kirja üldisemas tähenduses ristküliku pindala. Seda i-ndal lõigul oleva ristküliku pindala
kasutan oma mõtte jätkuks (Teine :math:`\sum`). Summa on ligikaudu võrdne tekkinud värviliste ristkülikute pindalade summaga.

Ehk teisisõnu - kui i rolli võtta 1, siis :math:`\Delta x_1` korda funktsiooni väärtus punktis :math:`\xi_1`
moodustub täpselt esimese ristküliku pindala. :math:`\Delta x` võrdub siis :math:`x_i - x_{i - 1}`.

Viimaks toome sisse veel ühe tähise: :math:`\lambda`, mis tähistab kõige pikemat osalõiku.

:math:`\lambda = max_{1 \leq i \leq n } \Delta x_i`

Ütlen, et minu esialgse suure kõvertrapetsi pindala on täpselt võrdne ristkülikute pindalade summaga.

:math:`S_{abBA} = \lim_{\lambda \rightarrow 0} \sum_{i = 1}^n f(\xi_i) \Delta x_i`

Piirväärtusest tuleb info, et seda juhul, kui lõigu peal on tehtud nii peenike jaotus, et ka kõige pikem osalõiguke on nullilähedane.

Seda juttu oli vaja, et saada järgmistel slaididel aru, et ristkülikute pindalade summal ja tema piirväärtusel on oma nimi.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/määratud_integraal.PNG
   :width: 300pt
   :align: center

Riemanni integraalsumma
^^^^^^^^^^^^^^^^^^^^^^^
Miks integreeritav Riemanni mõttes. Integraale on erinevaid.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/Riemanni_integraalsumma.PNG
   :width: 300pt
   :align: center

Määratud integraali definitsioon
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/määratud_integraal_definitsioon.PNG
   :width: 300pt
   :align: center

Määratud integraali arvutamine
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Kui on vaja leida määratud integraal, siis leitakse esmalt algfunktsioon ja seejärel asendatakse rajad.
Ülemise raja funktsioonist lahutatakse alumise raja funktsioon. Algfunktsiooni järel C-d pole, sest see pole algfunktsiooni üldavaldis.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/newton_liebnitz.PNG
   :width: 300pt
   :align: center

Määratud integraalile saab anda ka geomeetrilise tõlgenduse. Punane on :math:`x^3` väga suure suurenduse all.
X-telje peal on 2 ja 3 siniste joonte peal. Lõigul kahest kolmeni on lõik ülalpool x-telge.
Sellest tuleva kõvertrapetsi pindala on :math:`16 \frac{1}{4}`.

.. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/määratud_integraal_näide.PNG
   :width: 300pt
   :align: center

**NB! Kindlasti ei saa määratud integraaliga arvutada vaid pindala.**
Määratud integraali abil saab arvutada muid asju ka.

Tarvilikud ja piisavad tingimused funktsiooni integreeruvuseks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Tegelikkuses on täpselt integreeritavate funktsioonide hulk äärmiselt väike.
Siin on tulemuste kogu, mis ütleb millal integreerimine on võimalik.

-   Funktsiooni integreeruvuseks mingis lõigus on tarvilik, et ta oleks selles lõigus tõkestatud.
-   Lõigus pidev funktsioon on integreeruv selles lõigus.
-   Lõigus tõkestatud monotoonne funktsioon on integreeruv selles lõigus
-   Lõigus tõkestatud funktsioon, millel on lõplik arv katkevuspunkte, on integreeruv selles lõigus.
-   Kui funktsioonid :math:`f \text{ ja } g` on integreeruvad mingis lõigus, siis ka nende korrutis :math:`fg` on integreeruv selles lõigus.

Määratud integraali omadused
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.  **Aditiivsus**, kui :math:`c \in [a;b]`, siis

    :math:`\int_{a}^{b} f(x)dx = \int_{a}^{c} f(x)dx + \int_{b}^{c} f(x)dx`

    Võtad a ja b vahele jääva punkti c ja liidad sellest tekkivad kaks pindala.
    Integreerid a-st c-ni ja c-st b-ni. Sellest tuleb, et suure tüki pindala on
    esimene pindala pluss teine pindala.

    See omadus lubab meil teha määratud integraal katki ükskõik mis integreerimislõigu kohast
    ja kui sellest kohast jätkata, siis tulemus sellest ei muutu. Kasutatakse sageli keerukamate integraalide leidmisel.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/aditiivsus.PNG
       :width: 300pt
       :align: center

2.  **Lineaarsus**: kui :math:`\alpha , \beta \in R`, siis

    :math:`\int_{a}^{b} [\alpha f(x) + \beta g(x)]dx = \alpha \int_{b}^{a} f(x)dx + \beta \int_{a}^{b} g(x)dx`

    Võtab kokku kaks määramata integraali omadust. Kokku annavad lineaarsuse omaduse.

        1.  summa puhul võib igast liikmest integraali eraldi võtta.
        2.  konstantse teguri võib tuua integraalimärgi ette.

3.  **Monotoonsus**: kui funktsioonid f ja g on integreeruvad lõigus :math:`[a;b] \text{ ja } f(x) \geq g(x) \text{ iga } x \in [a;b]`, siis

    :math:`\int_{a}^{b} f(x)dx \geq \int_{a}^{b} g(x)dx`

    Selgitus: olgu meil kaks funktsiooni f ja g, mille puhul f-i väärtused on üldiselt väiksemad kui g väärtused.
    Juhul kui f-i väärtused on väiksemad g-st, siis üldjuhul tuleb määratud integraal f-ist väiksem kui määratud integraal g-st.
    Ehk kui võrratus kehtib funktsiooni puhul, siis kehtib ka määramata integraalide puhul.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/monotoonsus.PNG
       :width: 300pt
       :align: center

4.  Kui määratud integraali alumine ja ülemine raja on võrdsed siis võrdub integraal nulliga.

    :math:`\int_{a}^{a} f(x)dx = 0`

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/rajade_võrdsus.PNG
       :width: 300pt
       :align: center

5.  Kui vahetada määratud integraalis rajad, siis muutub märk integraali ees vastupidiseks.

    :math:`\int_{a}^{b} f(x)dx = -\int_{b}^{a} f(x)dx`

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/rajade_vahetus.PNG
       :width: 300pt
       :align: center

    Tavaliselt on rajades ülemine arv suurem ja alumine väiksem.

6.  Kui funktsioon :math:`f(x)` on paaris ja **rajad on teineteise vastandarvud**, siis

    :math:`\int_{-a}^{a} f(x)dx = 2\int_{0}^{a} f(x)dx`

    Paarisfunktsiooni graafik on sümmeetriline Y-telje suhtes.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/määratud_paaris.PNG
       :width: 300pt
       :align: center


    **Kindlasti peab teadma, et ainus trigonomeetria paarisfunktsioon on koosinusfunktsioon.**
    Hea ka teada, et paarisarvulise astendajaga funktsioonid on ka paarisfunktsioonid.
    Kui kahtled, siis võib alati kontrollida: :math:`f(-x) = f(x)`.

7.  Kui funktsioon :math:`f(x)` on paaritu ja **rajad on teineteise vastandarvud**, siis

    :math:`\int_{-a}^{a} f(x)dx = 0`

    Paaritu funktsioon on sümmeetriline koordinaatide alguspunktide suhtes.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/määratud_paaritu.PNG
       :width: 300pt
       :align: center

    Kuna graafik on allpool x-telge, siis saadav suurus on küll absoluutväärtuselt sama,
    mis teiselt poolt Y-telge saadav väärtus, kuid allapoole X-telge jääv osa tuleb miinusmärgiga.
    Kui panna kokku sama suur asi sama suure miinusmärgiga asjaga, siis tuleb 0.

    Tuleb kasuks ebamugavates integraalides. Funktsiooni on paaritu, kui :math:`f(-x) = -f(x)`
    Kindlasti peab teadma, et trigfunktsioonid :math:`sin \text{, } tan \text{ ja } cot` on paaritud funktsioonid.

**NB! Kui funktsioon pole ei paaris ega paaritu, siis tuleb integreerida tavareeglite järgi.**

Kui on paljude funktsioonide liitmine, siis tuleks iga funktsioon integreerida eraldi. Kui aga mõned on paaris ja mõned paaritud,
siis on vahel otstarbekas neid ka grupeerida.

Näide paaris- ja paaritu funktsiooniga
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/määratud_pp_näide.PNG
       :width: 300pt
       :align: center

Määratud integraali rakendused
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Määratud integraali sissetoomisest mäletame, kui meil on tegemist funktsiooniga, mille väärtused on positiivsed
ehk mille puhul graafik on ülevalpool x-telge, ja meil on kujund, mida piirab funktsiooni graafik, püstsirged ja X-telg.
Siis sellise kujundi pindala saame siis, kui lihtsalt vastavalt funktsiooni integreerime lõigu [a;b] piires.

:math:`S = \lim_{a}^{b} f(x)dx`

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pindala_leidmine_ktrapets.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pindala_leidmine_ktrapets_joonis.PNG
       :width: 300pt
       :align: center

Näide 1
^^^^^^^
Pindala leidmise ülesannete puhul tuleb alati esmalt teha joonis.
Joonise pealt saab palju infot. Kõigepealt näed ära kujundi, kas kujund üldse tekib ning mist küljest on kujund piiratud.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/mint_rakendused_näide1.PNG
       :width: 300pt
       :align: center

Pindala leidmise kõige tähtsam valem - üldvalem
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Üldvalemit saab kasutada siis, kui vaadeldava lõigu piires ülevalt ja alt piirav joon on üks ja seesama.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pindala_leidmine_üldvalem.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pindala_leidmine_üldvalem_joonis.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pindala_leidmine_üldvalem_joonis2.PNG
       :width: 300pt
       :align: center

Seda valemit läheb vaja sageli olukorras, kus saavad kokku kaks parabooli. Siis tuleb täiendavalt tabada,
et kõrval püstsirgeid pole, kuid mõttelised püstsirged asuvad paraboolide lõikepunktides.

Esimese hooga tuleb joonistada graafik ja tuvastada, kas meil üldse tekib kujund.
Seejärel tuleb leida lõikepunktid, et saada integraali rajad.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pindala_leidmine_üldvalem_näide.PNG
       :width: 300pt
       :align: center

Nüüd paneme kokku üldvalemi, lihtsustame ja leiame pindala. Lahutad ülemise graafiku funktsiooni alumise graafiku funktsioonist.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pindala_leidmine_üldvalem_näide2.PNG
       :width: 300pt
       :align: center

Mida teha siis, kui kõvertrapets asub allpool X-telge?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Üks variant on panna ümber absoluutväärtust tähistavad märgid

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pindala_leidmine_allpool_Xtelge.PNG
       :width: 300pt
       :align: center

Kuna kujund on allpool X-telge, siis panen absoluutväärtuste märgid. Teen oma arvutused ära.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pindala_leidmine_allpool_Xtelge2.PNG
       :width: 300pt
       :align: center

Teine variant on aru saada, et ülevalt piirab kujundit X-telg ehk :math:`y = 0` ja alt parabool ise ehk :math:`x^2 - 4x`.

:math:`S = \int_{0}^{4} (0 - (x^2 - 4x))dx`

Pindala leidmine, kui pinnatükk koosneb mitmest osast
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Ikka tarvis leida kujundi pindala, kuid piirajad on parabool ja koordinaattelg.

Esmalt tuleb teha joonis! Kui kujundeid on mitu, siis pole teha midagi muud kui pindalad leida ja lõppvastuses kokku panna.

Üldvalemit saab kasutada siis, kui vaadeldava lõigu piires ülevalt ja alt piirav joon on üks ja seesama.
Siin aga ülevalt ja alt piiravad jooned pole samad terves ulatuses. See on üks põhjus, miks tuleb võtta integraalid eraldi ja siis kokku panna.

Teiseks leiame parabooli lõikekohad X-teljega ja arvutame mõlema ala pindala. Lõpuks liidame.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/pindala_mitu_tk.PNG
       :width: 300pt
       :align: center

Määratud integraali ositi integreerimine
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
:math:`\int_{a}^{b} udv = uv |_{a}^{b} - \int_{a}^{b} vdu`,
:math:`uv|_{a}^{b} = u(b)v(b) - u(a)v(a)`

Kuna esialgsel integraalil on rajad, siis need tulevad kaasa.
Korrutis tuleb ka rajadega. Kõik muu on eelmise praktikumiga samas.
Ainuke uus asi on rajad ja see info tuleb teisele poole kaasa.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/määramata_ositi_näide.PNG
       :width: 300pt
       :align: center

Määratud integraali lahendamine muutuja vahetusega
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Kui ülesandes on aste, kaasa arvatud juur, siis on hea asendada astme alus ehk juurealuse asendan uue tähega.
See aga alati ei tööta, sest juure all on juba ainult x.

Siis tuleb mõtet laiendada. Võtan ruutjuure kaasa.

    .. image:: /studies/matemaatika/matemaatiline_analüüs/slaidid/mint_muutuja_vahetus.PNG
       :width: 300pt
       :align: center

Argumendi muutuja vahetusega tuleb vahetuse järel leida uued rajad.

**NB! Määratud integraalis muutujavahetusel tagasiasendust ei tehta!**
