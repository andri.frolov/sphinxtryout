Lineaaralgebra
##############
Sissejuhatus
============
Põhiteemad on maatriks, determinant ja lineaarseta võrrandisüsteemide lahendamine.

Maatriks
========
Maatriks on numbrite tabel, mis koosneb ridadest ja veergudest.

Maatrikseid tähistatakse enamasti suurte tähtedega ja nende
elemente indekseeritud väiketähtedega: :math:`A = (a_ij)`

Maatriksit, millel on **m rida ja n veergu**, nimetatakse **m × n dimensionaalseks**
ehk m × n mõõtmeliseks maatriksiks.

.. image:: /studies/matemaatika/lineaaralgebra/pildid/mndimensionaalne.PNG
   :width: 300pt
   :align: center

Maatriksi dimensioon
--------------------
Maatriksi dimensiooni märgitakse ka tähisega *dim*.
Kui maatriksil *A* on 2 rida ja 3 veergu, siis :math:`\dim A = 2 × 3`.

Maatriksi liigitus
------------------
Kui ridade arv m ja veergude arv n on võrdsed, siis nimetatakse maatriksit **ruutmaatriksiks**.

Ruutmaatriksit, millel vaid peadiagonaalil on nullist erinevaid
elemente ja kõik ülejäänud elemendid on nullid, nimetatakse **diagonaalmaatriksiks**.
Tähtis on meelde jätta, et *diagonaalmaatriksist räägime vaid ruutmaatriksi korral*.

NB! Peadiagonaalil võib ka olla nulle!!

.. image:: /studies/matemaatika/lineaaralgebra/pildid/diagonaalmaatriks.PNG
   :width: 150pt
   :align: center

Ühikmaatriks
------------
Lineaaralgebras nimetatakse ühikmaatriksiks n-ndat järku ruutmaatriksit, mille peadiagonaalil paiknevad 1-d ja mujal 0-d.
Ruutmaatriksit I nimetatakse (n-järku) ühikmaatriksiks, kui tema peadiagonaali elemendid võrduvad ühega ja kõik teised
elemendid võrduvad nulliga.

Ühikmaatriks on diagonaalmaatriks, mille peadiagonaali elemendid on 1-d.
Tema tähistus on I.

Ühikmaatriksiga korrutamisel maatriks ei muutu: iga ruutmaatriksi A korral kehtib: :math:`AI = IA = A`.

.. image:: /studies/matemaatika/lineaaralgebra/pildid/Ühikmaatriks.PNG
   :width: 300pt
   :align: center

Seega on ühikmaatriksite hulgas samasugune tähendus nagu arvul 1 skalaarsete suuruste hulgas.
Nullmaatriks
------------
Maatriksit nimetatakse nullmaatriksiks, kui kõik tema elemendid võrduvad nulliga.

Rea- ja veeruvektor
-------------------
Reavektor on maatriksi ühe rea elementidest moodustatud vektor.
Veeruvektor on maatriksi ühe veeru elementidest moodustatud vektor.

.. image:: /studies/matemaatika/lineaaralgebra/pildid/reajaveeruvektorid.PNG
   :width: 150pt
   :align: center

Maatriksite liitmine ja lahutamine
----------------------------------
Liita (lahutada) saab ainult samadimensionaalseid maatrikseid,
s.o. maatrikseid, millel ridade arv on võrdne ja veergude arv on
võrdne.

Maatriksite liitmisel (lahutamisel) liidetakse (lahutatakse) ühel ja
samal kohal olevad ehk samade indeksitega maatriksite
elemendid:

.. image:: /studies/matemaatika/lineaaralgebra/pildid/liitlah.PNG
   :width: 300pt
   :align: center

Liitmise-lahutamise omadusi
---------------------------
.. image:: /studies/matemaatika/lineaaralgebra/pildid/omadusi.PNG
   :width: 300pt
   :align: center


- Liitmine-lahutamine ei sõltu liidetavate järjekorrast

Maatriksi korrutamine skalaariga
--------------------------------
Skalaarseks suuruseks ehk skalaariks nimetatakse suurust, mis
on täielikult kirjeldatud ühe arvuga.

Maatriksi korrutamisel skalaariga korrutatakse selle skalaariga
läbi kõik maatriksi elemendid.

.. image:: /studies/matemaatika/lineaaralgebra/pildid/skalaar.PNG
   :width: 300pt
   :align: center

Skalaariga korrutamise omadusi
------------------------------
.. image:: /studies/matemaatika/lineaaralgebra/pildid/skalaariga_korrutamine.PNG
   :width: 300pt
   :align: center

Maatriksi transponeerimine
--------------------------
Transponeerimisel vahetavad maatriksi read ja veerud oma kohad.
Ehk võtad rea ja teed temast veeru.

Maatriksi A transponeerimisel saadud maatriksit nimetatakse
transponeeritud maatriksiks ja tähistatakse :math:`A^T`.

NB! Transponeerimisel maatriksi dimensioon muutub!

Näiteks 2 × 3 maatriksist saadakse 3 × 2 maatriks.
Üldiselt, kui :math:`\dim A = m × n \text{, siis } \dim A^T = n × m`.

Transponeerimise omadusi
------------------------
.. image:: /studies/matemaatika/lineaaralgebra/pildid/transponom.PNG
   :width: 300pt
   :align: center

Summa transponeerimisel võib teha maatriksi transponeerimised eraldi ja siis liita.
Kui on vaja transponeerida arv-korda-maatriks, siis võib kõigepealt maatriksi transponeerida ja siis korrutada.
Kui transponeeritud maatriksit veel transponeerida, siis saad esialgse maatriksi tagasi.

Sümmeetriline maatriks
----------------------
Maatriksit, mis transponeerimisel ei muutu :math:`A^T = A`, nimetatakse
sümmeetriliseks maatriksiks (sümmeetriliseks peadiagonaali suhtes)

NB! Sümmeetriline saab olla vaid ruutmaatriks.

Rea- ja veeruvektorite skalaarkorrutis
--------------------------------------
.. image:: /studies/matemaatika/lineaaralgebra/pildid/skalaarkorr.PNG
   :width: 300pt
   :align: center

Skalaarkorrutise leidmisel korrutatakse reavektori elemendid
vastavate elementidega veeruvektorist ja saadud korrutised
liidetakse.

**Rea- ja veeruvektori skalaarkorrutist saab leida vaid siis, kui I teguri veergude arv = II teguri ridade arv.**

Näide:

.. image:: /studies/matemaatika/lineaaralgebra/pildid/reaveeruvektoriteskalaarkorrutis.PNG
   :width: 300pt
   :align: center

Maatriksite korrutamine
-----------------------
Maatrikseid A ja B saab korrutada, kui A veergude arv = B ridade arv.
St, et **maatriksite korrutamine ei ole alati võimalik**.

Uue maatriksi mõõt sõltub välimistest mõõtudest (punasega):
R  V  <---> R  V

Korrutamisel on visuaalseks abiks transponeerimine.

.. image:: /studies/matemaatika/lineaaralgebra/pildid/korrutamine.PNG
   :width: 300pt
   :align: center

NB! Kahe maatriksi sisemised mõõdud peavad langema kokku (sinine)!

.. image:: /studies/matemaatika/lineaaralgebra/pildid/korrutamine_näide1.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/lineaaralgebra/pildid/korrutamine_näide2.PNG
   :width: 300pt
   :align: center

Käi A esimese reaga läbi nii palju B veerge kui B-s on, siis liigu A järgmise rea juurde ja repeat.

AB =/= BA
---------
Maatriksite korrutamine ei ole kommutatiivne.

.. image:: /studies/matemaatika/lineaaralgebra/pildid/CAB.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/lineaaralgebra/pildid/DBA.PNG
   :width: 300pt
   :align: center

.. image:: /studies/matemaatika/lineaaralgebra/pildid/DBA2.PNG
   :width: 300pt
   :align: center

Determinandid
=============
**Determinandist saame rääkida vaid ruutmaatriksi korral!**

N-järgu ruutmaatriksi determinant
---------------------------------
Kaks asja - determinant on arv ja temast
N-järku ruutmaatriksi A determinandiks nimetakse arvu
:math:`det A = |A|`, mida tähistatakse

.. image:: /studies/matemaatika/lineaaralgebra/pildid/determinant.PNG
   :width: 300pt
   :align: center

ja arvutatakse vastava eeskirja põhjal.

Determinant on teisendus (teatud liiki funktsioon), mis seab igale
ruutmaatriksile vastavusse ühe kindla arvu.

Kui me räägime determinandi elementidest, ridadest ja veergudest,
siis me mõtleme selle all maatriksi elemente, ridu ja veerge.

Tähistuses arvu all paremas nurgas on esmalt rea, siis veeru number.

NB! Kui räägime determinandi reast ja veerust, siis räägime
tegelikult maatriksi reast ja veerust.

Teist järku determinant
-----------------------
Teist järku maatriksile vastav teist järku determinant avaldub
kujul:

.. image:: /studies/matemaatika/lineaaralgebra/pildid/teist_järku_determinant.PNG
   :width: 300pt
   :align: center

Kolmandat järku determinant, Sarruse reegel
-------------------------------------------
.. image:: /studies/matemaatika/lineaaralgebra/pildid/kolmandat_järku_determinant.PNG
   :width: 300pt
   :align: center

Trikk on selles, et diagonaalid on plusside puhul paralleelsed paremalt alla
ja miinuste puhul alt üles.

Kui peadiagonaali all on kõik nullid
------------------------------------
Kui peadiagonaali all on kõik nullid,
siis võrdub determinant peadiagonaali korrutisega.

.. image:: /studies/matemaatika/lineaaralgebra/pildid/peadiagonaal_nullid.PNG
   :width: 300pt
   :align: center

Miinor
------
Maatriksi A elemendi :math:`a_{ij}` miinor :math:`M_{ij}` on (n-1) järku determinant,
mis saadakse, kui eemaldame maatriksist A seda elementi läbiva rea ja veeru.

.. image:: /studies/matemaatika/lineaaralgebra/pildid/miinor.PNG
   :width: 300pt
   :align: center

Eesmärk - arvutada kõrgemat järku determinante.

Determinandi arendus rea järgi
------------------------------
**Meie kursuses on tähtsaim saada selgeks arendus rea järgi!**

Kasutades miinoreid võib n-järku determinandi arendada ritta kas rea või veeru järgi.

Determinandi arendus i-nda rea järgi (summeerimine toimub
üle veeruindeksite, rida on fikseeritud):

.. image:: /studies/matemaatika/lineaaralgebra/pildid/arendus_rea_järgi.PNG
   :width: 150pt
   :align: center

See summa on ühtlasi kõrgemat järku determinandi definitsioon.

Kõrgemat järku (n ≥ 2) ruutmaatriksi determinandiks
nimetatakse summat:

.. image:: /studies/matemaatika/lineaaralgebra/pildid/kõrgema_järgu_ruutmaatriksi_determinant.PNG
   :width: 150pt
   :align: center

kus :math:`M_{ij}` on elemendile :math:`a_{ij}` vastav (n-1)-järku determinant.

Kolmandat järku determinandi arendus
------------------------------------
.. image:: /studies/matemaatika/lineaaralgebra/pildid/arendus_esimene_rida.PNG
   :width: 300pt
   :align: center

Korrutame astendajaga (-1)-e argumendi iseenda ja tema miinoriga.

Näide:

.. image:: /studies/matemaatika/lineaaralgebra/pildid/determinandi_arenduse_näide.PNG
   :width: 300pt
   :align: center

Determinandi arendus veeru järgi
--------------------------------
Determinandi väärtus ei muutu, kui arendamine toimub ridade asemel veergudega.

Determinandi arendus j-nda veeru järgi (summeerimine toimub üle reaindeksite, veerg on fikseeritud):

.. image:: /studies/matemaatika/lineaaralgebra/pildid/arendus_veeru_järgi.PNG
   :width: 300pt
   :align: center

Märkus: arendamisel on kasulik valida selline rida või veerg, kus on palju nulle.

Näide:

.. image:: /studies/matemaatika/lineaaralgebra/pildid/arendus_veeru_järgi_näide.PNG
   :width: 300pt
   :align: center

Alamdeterminant
---------------
Kasutatakse rea järgi arendusel. Alamdeterminandi puhul pole elementi ennast.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/alamdeterminant.PNG
       :width: 300pt
       :align: center

Determinandi omadused
---------------------
    1. Determinandi väärtus ei muutu, kui tema read ja veerud omavahel ümber paigutada ehk :math:`|A| = |A^T|`.
       Järeldus: Seega kõik omadused, mis kehtivad determinandi ridade kohta, kehtivad ka veergude kohta.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/determinand_väärtus.PNG
       :width: 300pt
       :align: center

    2. Kahe rea (või veeru) vahetamisel muutub determinandi märk vastupidiseks.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/determinandi_märk.PNG
       :width: 300pt
       :align: center

    3. Kahe võrdse rea (või veeru) puhul on determinandi väärtus null.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/determinandi_null.PNG
       :width: 300pt
       :align: center

    4. Kui determinandi mingis reas (või veerus) on kõik elemendid nullid, siis determinandi väärtus võrdub nulliga

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/determinandi_arenduse_nullid.PNG
       :width: 300pt
       :align: center

    5. Mistahes rea (või veeru) elementides esineva ühise kordaja võib tuua kordajaks determinandi sümboli ette:

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/determinant_kordaja.PNG
       :width: 300pt
       :align: center

    Determinant siin pisut erineb maatriksist, sest seal tuli korrutada läbi kõik liikmed.
    Determinandis piisab ühest reast.

    See trikk on hea suurte arvudega arvutuste vältimiseks.


    6. Kui determinandi mingi rea (või veeru) elemendid on kahe liidetava summad, siis avaldub determinant kahe determinandi summana

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/determinandi_summa.PNG
       :width: 300pt
       :align: center

    7. Determinandi väärtus ei muutu, kui ühe rea (või veeru)
       elementidele liita ühe ja sama teguriga korrutatud teise rea
       (või veeru) elemendid.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/determinant_sama_teguri_korrutis.PNG
       :width: 300pt
       :align: center

    **See on omadustest ilmselt kõige kasulikum!**

Näited:

    Otsin nulle või võimalikult väikseid arve.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/omadus_näide.PNG
       :width: 300pt
       :align: center

Meeldetuletus - ühikmaatriks
----------------------------
Ruutmaatriksit I nimetatakse (n-järku) ühikmaatriksiks, kui tema peadiagonaali elemendid võrduvad ühega ja kõik teised
elemendid võrduvad nulliga.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/ühikmaatriks.PNG
       :width: 300pt
       :align: center

Iga ruutmaatriksi A korral kehtib: AI = IA = A.

Pöördväärtus vs Pöördmaatriks
-----------------------------
    .. image:: /studies/matemaatika/lineaaralgebra/pildid/pöördmaatriks.PNG
       :width: 300pt
       :align: center

Pöördmaatriks
-------------
Pöördmaatriks on vaid ruutmaatriksitel!

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/pöördmaatriks2.PNG
       :width: 300pt
       :align: center

Regulaarne ja singulaarne maatriks
----------------------------------
Ruutmaatriksit, mille determinant ei võrdu nulliga, nimetatakse
**regulaarseks**.

Ruutmaatriksit, mille determinant võrdub nulliga, nimetatakse
**singulaarseks**.

Igal maatriksil ei ole pöördmaatriksit. **Pöördmaatriks on olemas vaid regulaarsetel maatriksitel**.

Pöördmaatriksi leidmine
-----------------------
    .. image:: /studies/matemaatika/lineaaralgebra/pildid/pöördmaatriks_leidmine.PNG
       :width: 300pt
       :align: center

Ülesanne:
    1. Leiame maatriksi A determinandi.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/pöördmaatriks_ülesanne_1.PNG
       :width: 300pt
       :align: center

    NB! Ära unusta, et pöördmaatriks on olemas vaid siis, kui maatriks on ruutmaatriks
    ja tema determinant pole null.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/pöördmaatriks_ülesanne_2.PNG
       :width: 300pt
       :align: center

    2. Leiame alamdeterminandid.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/alamdeterminant_1n.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/alamdeterminant_2n.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/alamdeterminant_3n.PNG
       :width: 300pt
       :align: center

    3. Panen kirja pöördmaatriksi.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/pöördmaatriks_ülesanne_3.PNG
       :width: 300pt
       :align: center

Süsteemi esitamine maatrikskujul
--------------------------------

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/süsteem-võrrandi-kujul.PNG
       :width: 300pt
       :align: center

Maatriksvõrrandi lahend
-----------------------
Kuidas lahendada võrrandisüsteemi? Leiame vasaku poole kordajatest moodustatud maatriksi pöördmaatriks ja korrutada see vabaliikmete veeruga.
Tulemuseks on lahendite hulk.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/maatriksvõrrandi_lahendamine.PNG
       :width: 300pt
       :align: center

Näide:

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/maatriksvõrrand_näide_1.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/maatriksvõrrand_näide_1b.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/maatriksvõrrand_näide_1c.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/maatriksvõrrand_näide_1d.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/maatriksvõrrand_näide_1e.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/maatriksvõrrand_näide_1f.PNG
       :width: 300pt
       :align: center

Maatriksi miinor
----------------

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/maatriksi_miinor.PNG
       :width: 300pt
       :align: center

Miinorite järgud
^^^^^^^^^^^^^^^^

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/miinorite_järgud.PNG
       :width: 300pt
       :align: center

Maatriksi astak
---------------
Sellega iseloomustatakse seda, kas võrrandisüsteem on lahenduv ning kui palju on lahendeid.

Maatriksi A astakuks nimetatakse selle maatriksi nullist erinevate miinorite kõrgeimat järku.

Astak on nullist erineva determinandi järk.

Tähistus: rank(A) või r(A) või r.

:math:`r \leq min(m, n)`

Teadma saamaks, mis on maatriksi astak, tuleb mõelda milline on kõrgeimat järku nullist erinev determinant
selles maatriksis ja selle determinandi järk annab meile selle astaku. m on ridade arv ja n veergude arv.

Näide:

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/astak_näide.PNG
       :width: 300pt
       :align: center

Lineaarvõrrandisüsteemid
------------------------
Võrrandisüsteem on lineaarne, kui tundmatud esinevad vaid esimeses astmes.

:math:`a_{ij} (i = 1, ..., m \text{ ja } j = 1, ..., n)` nimetatakse võrrandisüsteemi kordajateks
ja arve :math:`b_1, b_2, ..., b_m` vabaliikmeteks.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/lineaarvõrrandisüsteem.PNG
       :width: 300pt
       :align: center

Lineaarvõrrandisüsteemi maatrikskuju
------------------------------------
Võrrandisüsteemi kordajatest saab moodustada maatriksi, mida nimetatakse **süsteemi maatriksiks**.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/lvs_maatrikskuju.PNG
       :width: 150pt
       :align: center

Veeruvektoreid x ja b nimetatakse vastavalt tundmatute ja vabaliikmete vektoriks.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/tundm_vabal_vektor.PNG
       :width: 150pt
       :align: center

Lineaarvõrrandisüsteem on maatrikskujul antav võrdusena Ax = b.

Võrrandisüsteemi lahend
-----------------------

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/lahend.PNG
       :width: 300pt
       :align: center

Kronecker-Capelli teoreem
-------------------------
Oluline ei ole determinandi väärtus, vaid see kas ta on nullist erinev.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/Kronecker-Capell.PNG
       :width: 300pt
       :align: center

Crameri peajuht
---------------

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/Crameri_peajuht.PNG
       :width: 300pt
       :align: center

Näide:

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/cramer_näide.PNG
       :width: 300pt
       :align: center

Maatriksi elementaarteisendused
-------------------------------

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/maatriksi_elementaarteisendused.PNG
       :width: 300pt
       :align: center

Astaku leidmine elementaarteisenduste abil
------------------------------------------
Elementaarteisendusi kasutades maatriksi astak ei muutu.
Reeglina kasutatakse seda meetodit.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/elementt_astak.PNG
       :width: 300pt
       :align: center

NB! Nool on tähtis. Võrdusmärki tõmmata ei tohi, sest elementaarteisendus muudab maatriksit.
Astak ega determinant ei muutu, aga maatriks ise muutub.

Üldsoovitus on, et juhtelement tuleks teha esmalt 1-ks. Sellega on hiljem mugav toimetada.

Näide:

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/elementt_astak.PNG
       :width: 300pt
       :align: center

Kui 5xX maatriksis on viimane rida nullid, siis tähendab et 5. järgu determinant on null
ja liigume edasi 4. rea peale. Kui ka seal on nullid, siis 3.-le. Uuri seal, kus pole kõik nullid.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/elementt_astak2.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/elementt_astak3.PNG
       :width: 300pt
       :align: center

Kordamine - Võrrandisüsteemi kolmnurkne kuju
--------------------------------------------
Kolmnurkse kuju saamiseks tuleb liita-lahutada-korrutada-jagada kuni viimase tundmatuni nulliks.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/vs_kolmnurkne_kuju.PNG
       :width: 300pt
       :align: center

Näide:

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/vs_kolmnurkne_kuju_näide.PNG
       :width: 300pt
       :align: center

Oleks saanud lõpetada ka vasakpoolse punase ringi peal.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/vs_kolmnurkne_kuju_näide2.PNG
       :width: 300pt
       :align: center

Eraldav kriips. Mõned autorid kirjutavad selle ja mõned mitte, ta on vabatahtlik. Selgem on kirjutada ja parem andmeid lugeda.

Näide:

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/vs_näide1.PNG
       :width: 150pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/vs_näide1a.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/vs_näide1b.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/vs_näide1c.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/vs_näide1d.PNG
       :width: 300pt
       :align: center


Gaussi elimineerimismeetod
--------------------------
Olgu meil vaja lahendada lineaarvõrrandisüsteem Ax = b.
Gaussiga lahendamiseks:

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/gauss.PNG
       :width: 300pt
       :align: center

Näide 1:

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/gauss_näide1a.PNG
       :width: 300pt
       :align: center

n on tundmatute arv, m on võrrandite arv ja r on astak.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/gauss_näide.PNG
       :width: 300pt
       :align: center

Näide 2:

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/gauss_näide2.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/gauss_näide2a.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/gauss_näide2b.PNG
       :width: 300pt
       :align: center

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/gauss_näide2c.PNG
       :width: 300pt
       :align: center

siin r = 2, m = 3, n = 3.

n - r = 3 - 2 = 1;

Näide ühe võrrandiga süsteemist.

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/lahendite_arv.PNG
       :width: 300pt
       :align: center

Homogeenne lineaarvõrrandisüsteem
---------------------------------

    .. image:: /studies/matemaatika/lineaaralgebra/pildid/homogeenne_lvs.PNG
       :width: 300pt
       :align: center

Näide:

